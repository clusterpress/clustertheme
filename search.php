<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package ClusterTheme
 * @subpackage search
 *
 * @since 1.0.0
 */

get_header(); ?>

<div id="headline">
	<div class="wrapper">
		<?php if ( clusterpress_have_posts() ) : ?>
			<h1 class="page-title"><?php printf( esc_html__( 'Résultat(s) de la recherche pour : %s', 'clustertheme' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
		<?php else : ?>
			<h1 class="page-title"><?php esc_html_e( 'Aucun contenu n\'a été trouvé', 'clustertheme' ); ?></h1>
		<?php endif; ?>
	</div>
</div>

<div class="wrapper">

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="wrapper">

			<?php if ( clusterpress_have_posts() ) :
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/**
					 * Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called content-search.php and that will be used instead.
					 */
					get_template_part( 'template-parts/content', 'search' );

				endwhile;

				the_posts_navigation( clustertheme_navigation_args() );

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>

			</div><!-- .wrapper -->
		</main><!-- #main -->
	</section><!-- #primary -->

	<?php get_sidebar(); ?>
</div><!-- .wrapper -->

<?php get_footer();
