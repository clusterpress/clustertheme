<?php
/**
 * The header for the mulstisite signup page.
 *
 * @package ClusterTheme
 * @subpackage header-wp-signup
 *
 * @since 1.0.0
 */

get_template_part( 'header' );
?>

		<div id="headline">
			<div class="wrapper">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</div>
		</div>

		<div id="primary" class="content-area">
			<div class="wrapper">
				<main id="main" class="site-main" role="main">
