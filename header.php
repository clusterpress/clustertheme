<?php
/**
 * The template to display the ClusterTheme's header.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ClusterTheme
 * @subpackage header
 *
 * @since 1.0.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Allez directement au contenu', 'clustertheme' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="wrapper">
			<div class="site-branding">

				<?php clustertheme_custom_logo(); ?>

				<?php
				if ( is_front_page() && is_home() ) : ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php clustertheme_blogname();?></a></h1>
				<?php else : ?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php clustertheme_blogname();?></a></p>
				<?php
				endif;

				$description = get_bloginfo( 'description', 'display' );
				if ( $description || is_customize_preview() ) : ?>
					<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
				<?php
				endif; ?>
			</div><!-- .site-branding -->

			<nav id="site-navigation" class="main-navigation" role="navigation">
				<button class="menu-toggle" aria-controls="header-menu" aria-expanded="false">
					<span class="dashicons dashicons-menu"></span>
					<span class="screen-reader-text"><?php esc_html_e( 'Menu', 'clustertheme' ); ?></span>
				</button>
				<?php wp_nav_menu( clustertheme_get_nav_args() ); ?>
			</nav><!-- #site-navigation -->
		</div><!-- .wrapper -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
