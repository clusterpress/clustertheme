<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ClusterTheme
 * @subpackage single
 *
 * @since 1.0.0
 */

get_header(); ?>

	<div id="headline">
		<div class="wrapper">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</div>

	<div id="primary" class="content-area">
		<div class="wrapper">
			<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', get_post_format() );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

				the_post_navigation( clustertheme_navigation_args( 'post' ) );

			endwhile; // End of the loop.
			?>

			</main><!-- #main -->
		</div><!-- .wrapper -->
	</div><!-- #primary -->

<?php get_footer();
