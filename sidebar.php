<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ClusterTheme
 * @subpackage sidebar
 *
 * @since 1.0.0
 */

$sidebar = sprintf( 'sidebar-%s', esc_attr( clustertheme_locale() ) );

if ( ! is_active_sidebar( $sidebar ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area" role="complementary">
	<div class="wrapper">
		<?php dynamic_sidebar( $sidebar ); ?>
	</div>
</aside><!-- #secondary -->
