<?php
/**
 * The template for displaying comments.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ClusterTheme
 * @subpackage comments
 *
 * @since 1.0.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php
				printf( // WPCS: XSS OK.
					esc_html( _nx( 'Une réponse à &ldquo;%2$s&rdquo;', '%1$s réponses à &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'clustertheme' ) ),
					number_format_i18n( get_comments_number() ),
					'<span>' . get_the_title() . '</span>'
				);
			?>
		</h2>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
			<h2 class="screen-reader-text"><?php esc_html_e( 'Navigation des commentaires', 'clustertheme' ); ?></h2>
			<div class="nav-links">

				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Anciens commentaires', 'clustertheme' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( esc_html__( 'Commentaires récents', 'clustertheme' ) ); ?></div>

			</div><!-- .nav-links -->
		</nav><!-- #comment-nav-above -->
		<?php endif; // Check for comment navigation. ?>

		<ol class="comment-list">
			<?php
				add_filter( 'edit_comment_link', 'clustertheme_edit_comment_link', 10, 3 );
				add_filter( 'comment_reply_link_args', 'clustertheme_comment_reply_link_args', 10, 1 );

				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 45,
				) );

				remove_filter( 'edit_comment_link', 'clustertheme_edit_comment_link', 10, 3 );
				remove_filter( 'comment_reply_link_args', 'clustertheme_comment_reply_link_args', 10, 1 );
			?>
		</ol><!-- .comment-list -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
			<h2 class="screen-reader-text"><?php esc_html_e( 'Navigation des commentaires', 'clustertheme' ); ?></h2>
			<div class="nav-links">

				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Anciens commentaires', 'clustertheme' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( esc_html__( 'Commentaires récents', 'clustertheme' ) ); ?></div>

			</div><!-- .nav-links -->
		</nav><!-- #comment-nav-below -->
		<?php
		endif; // Check for comment navigation.

	endif; // Check for have_comments().


	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

		<p class="no-comments"><?php esc_html_e( 'L\'ajout de commentaires n\'est pas autorisé pour ce contenu.', 'clustertheme' ); ?></p>
	<?php
	endif;

	comment_form();
	?>

</div><!-- #comments -->
