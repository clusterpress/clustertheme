<?php
/**
 * Template for displaying search forms in ClusterTheme
 *
 * @package ClusterTheme
 * @subpackage searchform
 *
 * @since 1.0.0
 */

?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label for="<?php echo $unique_id; ?>">
		<span class="screen-reader-text"><?php echo _x( 'Rechercher :', 'label', 'clustertheme' ); ?></span>
	</label>
	<input type="search" id="<?php echo $unique_id; ?>" class="search-field" placeholder="<?php echo esc_attr_x( 'Rechercher &hellip;', 'placeholder', 'clustertheme' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	<button type="submit" class="search-submit">
		<span class="dashicons dashicons-search"></span>
		<span class="screen-reader-text"><?php echo _x( 'Rechercher', 'submit button', 'clustertheme' ); ?></span>
	</button>
</form>
