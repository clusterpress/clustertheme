<?php
/**
 * The sidebar containing the ClusterPress widget area.
 *
 * @package ClusterTheme
 * @subpackage sidebar-clusterpress
 *
 * @since 1.0.0
 */

$sidebar = sprintf( 'sidebar-clusterpress-%s', esc_attr( clustertheme_locale() ) );

if ( ! is_active_sidebar( $sidebar ) ) {
	// Fall back on regular sidebar.
	get_template_part( 'sidebar' );

// Use the ClusterPress Sidebar.
} else {
	?>

	<aside id="secondary" class="widget-area" role="complementary">
		<div class="wrapper">
			<?php dynamic_sidebar( $sidebar ); ?>
		</div>
	</aside><!-- #secondary -->

<?php
}
