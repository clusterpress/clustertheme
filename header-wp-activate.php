<?php
/**
 * The header for the mulstisite activate page.
 *
 * It uses the wp-signup header.
 *
 * @package ClusterTheme
 * @subpackage header-wp-activate
 *
 * @since 1.0.0
 */

get_template_part( 'header-wp-signup' );
