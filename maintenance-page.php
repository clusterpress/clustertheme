<?php
/**
 * The Template to displaye the Maintenance page.
 *
 * @package ClusterTheme
 * @subpackage maintenance-page
 *
 * @since 1.0.0
 */

get_header( 'maintenance' ); ?>

	<main id="main" class="site-main" role="main">

			<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'page' );

				endwhile; // End of the loop.
			?>

	</main><!-- #main -->

</div><!-- .site -->
</body>
</html>
