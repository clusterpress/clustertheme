ClusterTheme
============

> Tailored for Multisite and ClusterPress, ClusterTheme is the theme that shapes the [Cluster.Press](https://cluster.press) tribe site, the contributors tribe of the ClusterPress open source projects. ClusterTheme introduces an email template for WordPress-generated notifications you can customize from the Customizer. Users can set their preferred language between English and French to browse the site. The static homepage can include scrollable panels. You can display a maintenance page while polishing your settings. And of course, this theme is ClusterPress ready!


Credits
-------

* Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
* External Library for the One Page Scroll home page feature http://www.thepetedesign.com, (C) Pete Rojwongsuriya, [GPLv3](http://www.gnu.org/licenses/gpl-3.0.html)
