<?php
/**
 * ClusterTheme Maintenance mode
 *
 * @package ClusterTheme\inc
 * @subpackage maintenance
 *
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Maintenance mode Class
 * let's work with no pressure to finish the job..
 *
 * @since  1.0.0
 */
class ClusterTheme_Maintenance {

	protected static $instance = null;

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		$this->template = 'maintenance-page';
		$this->setup_hooks();
	}

	/**
	 * Start new instance or use existing.
	 *
	 * @since 1.0.0
	 */
	public static function start() {
		if ( null == self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Set hooks
	 *
	 * @since 1.0.0
	 */
	public function setup_hooks() {
		if ( is_admin() || current_user_can( 'manage_options' ) ) {
			return;
		}

		// Neutralize signups.
		add_filter( 'pre_site_option_registration', '__return_zero'                    );
		add_action( 'before_signup_header',         array( $this, 'redirect_home' ), 1 );
		add_action( 'activate_header',              array( $this, 'redirect_home' ), 1 );

		// Use the maintenance template
		add_filter( 'template_include', array( $this, 'template_include' ), 12    );

		// Make sure nobody is filtering this anymore.
		remove_all_filters( 'posts_pre_query' );

		// Set the maintenance post.
		add_filter( 'posts_pre_query',  array( $this, 'posts_pre_query' ),  10, 2 );
	}

	/**
	 * Make sure signup and activation page are redirected.
	 *
	 * @since 1.0.0
	 */
	public function redirect_home() {
		wp_redirect( home_url() );
		exit();
	}

	/**
	 * Get the maintenance template.
	 *
	 * @since 1.0.0
	 */
	public function template_include( $template = '' ) {
		$template = sprintf( '%1$s/%2$s.php', get_template_directory(), $this->template );

		return $template;
	}

	/**
	 * Set the Post for the maintenance page
	 *
	 * @since 1.0.0
	 *
	 * @param  null   $return   A null value to use the regular WP Query.
	 * @param  WP_Query $wq     The WP Query object.
	 * @return null|array       Null if not on front end.
	 *                          An array containing a Maintenance Post otherwise.
	 */
	public function posts_pre_query( $return = null, WP_Query $wq ) {
		if ( ! $wq->is_main_query() || true === $wq->get( 'suppress_filters' ) || is_admin() ) {
			return $return;
		}

		$wq = clustertheme_alter_posts_query( $wq, array(
			'post_type'      => 'maintenance',
			'post_title'     => __( 'En cours de maintenance', 'clustertheme' ),
			'post_content'   => sprintf( '
					<h1 class="maintenance-title"><span class="dashicons dashicons-admin-tools"></span> %1$s</h1>
					<p>%2$s</p>
				',
				clustertheme_get_blogname(),
				esc_html__( 'est en cours de maintenance et sera prochainement disponible.', 'clustertheme' )
			),
		) );

		if ( isset( $wq->clustertheme_null ) || empty( $wq->posts ) ) {
			unset( $wq->clustertheme_null );
			return $return;
		}

		return $wq->posts;
	}
}
