<?php
/**
 * ClusterTheme functions.
 *
 * @package ClusterTheme\inc
 * @subpackage functions
 *
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Get Theme version.
 *
 * @since 1.0.0
 *
 * @return string the Theme version.
 */
function clustertheme_version() {
	return clustertheme()->version;
}

/**
 * Get Theme directory.
 *
 * @since 1.0.0
 *
 * @return string the Theme directory.
 */
function clustertheme_dir() {
	return clustertheme()->dir;
}

/**
 * Get Theme include directory.
 *
 * @since 1.0.0
 *
 * @return string the Theme include directory.
 */
function clustertheme_inc_dir() {
	return clustertheme()->inc_dir;
}

/**
 * Get Theme css directory.
 *
 * @since 1.0.0
 *
 * @return string the Theme css directory.
 */
function clustertheme_css_dir() {
	return clustertheme()->css_dir;
}

/**
 * Get Theme url.
 *
 * @since 1.0.0
 *
 * @return string the Theme url.
 */
function clustertheme_url() {
	return clustertheme()->url;
}

/**
 * Get Theme Javascript URL.
 *
 * @since 1.0.0
 *
 * @return string the Theme Javascript URL.
 */
function clustertheme_js_url() {
	return clustertheme()->js_url;
}

/**
 * Get Theme Stylesheets URL.
 *
 * @since 1.0.0
 *
 * @return string the Theme Stylesheets URL.
 */
function clustertheme_css_url() {
	return clustertheme()->css_url;
}

/**
 * Is the site in maintenance mode ?
 *
 * @since 1.0.0
 *
 * @return bool True if the site is in maintenance mode. False otherwise.
 */
function clustertheme_is_maintenance() {
	return clustertheme()->is_maintenance;
}

/**
 * Are we on the main WordPress site?
 *
 * @since 1.0.0
 *
 * @return bool True if on the main site. False otherwise.
 */
function clustertheme_is_main_site() {
	return apply_filters( 'clustertheme_is_main_site', clustertheme()->is_main_site );
}

/**
 * Get the suffix for Javascript and CSS files.
 *
 * @since 1.0.0
 *
 * @return string The file suffix.
 */
function clustertheme_js_css_suffix() {
	$min = '.min';

	if ( defined( 'SCRIPT_DEBUG' ) && true === SCRIPT_DEBUG )  {
		$min = '';
	}

	return apply_filters( 'clustertheme_js_css_suffix', $min );
}

/**
 * Get the editor styles for the WP Editor use.
 *
 * @since  1.0.0
 *
 * @return array The list of editor styles.
 */
function clusterpress_get_editor_styles() {
	$min           = clustertheme_js_css_suffix();
	$editor_styles = array(
		sprintf( 'css/editor-style%s.css', $min )
	);

	$colorscheme = get_theme_mod( 'colorscheme', 'default' );

	// Load the selected colorscheme.
	if ( 'default' !== $colorscheme ) {
		$editor_styles[] = sprintf( 'css/%1$s%2$s.css', sanitize_file_name( $colorscheme ), $min );
	}

	return (array) apply_filters( 'clusterpress_get_editor_styles', $editor_styles );
}

/**
 * Are we previewing the email template ?
 *
 * @since 1.0.0
 *
 * @return  bool True if previewing the email template. False otherwise.
 */
function clusterpress_is_email_preview() {
	return is_customize_preview() && (int) get_option( 'clustertheme_email_id' ) === (int) get_the_ID();
}

/**
 * Should we use the front-page.php template.
 *
 * The front-page.php template is only used for static home pages.
 *
 * @since 1.0.0
 *
 * @param  string $template The path to the template (required).
 * @return string           An empty string or the path to the template.
 */
function clustertheme_front_page_template( $template ) {
	return is_home() ? '' : $template;
}

/**
 * Register the Javascripts and Stylesheets.
 *
 * @since 1.0.0
 */
function clustertheme_register_cssjs() {
	$styles = apply_filters( 'clustertheme_register_css', array(
		'clustertheme-main' => array(
			'file' => '%1$smain%2$s.css',
			'url'  => '',
			'deps' => array( 'dashicons' ),
		),
		'clustertheme-layout' => array(
			'file' => '%1$scontent-sidebar%2$s.css',
			'url'  => '',
			'deps' => array( 'clustertheme-main' ),
		),
		'clustertheme-onepage' => array(
			'file' => '%1$sfrontpage%2$s.css',
			'url'  => '',
			'deps' => array( 'clustertheme-layout' ),
		),
		'clustertheme-maintenance' => array(
			'file' => '%1$smaintenance%2$s.css',
			'url'  => '',
			'deps' => array( 'dashicons' ),
		),
		'clustertheme-dark' => array(
			'file' => '%1$sdark%2$s.css',
			'url'  => '',
			'deps' => array(),
		),
		'clustertheme-red' => array(
			'file' => '%1$sred%2$s.css',
			'url'  => '',
			'deps' => array(),
		),
	) );

	$version = clustertheme_version();
	$min     = clustertheme_js_css_suffix();

	foreach ( $styles as $ks => $vs ) {
		if ( empty( $vs['url'] ) ) {
			$vs['url'] = clustertheme_css_url();
		}

		if ( clustertheme_is_maintenance() && ! current_user_can( 'manage_options' ) && false === array_search( $ks, array( 'clustertheme-maintenance', 'clustertheme-dark', 'clustertheme-red' ) ) ) {
			continue;
		}

		wp_register_style( $ks, sprintf( $vs['file'], trailingslashit( $vs['url'] ), $min ), $vs['deps'], $version );
	}

	$js_url = clustertheme_js_url();

	$scripts = apply_filters( 'clustertheme_register_js', array(
		'clustertheme-navigation' => array(
			'file'   => '%1$snavigation%2$s.js',
			'url'    => $js_url,
			'deps'   => array(),
			'footer' => true,
		),
		'clustertheme-skip-link-focus-fix' => array(
			'file'   => '%1$sskip-link-focus-fix%2$s.js',
			'url'    => $js_url,
			'deps'   => array(),
			'footer' => true,
		),
		'clustertheme-onepage' => array(
			'file'   => '%1$sjquery.onepage-scroll%2$s.js',
			'url'    => $js_url,
			'deps'   => array( 'jquery', 'clustertheme-navigation', 'clustertheme-skip-link-focus-fix' ),
			'footer' => true,
		),
	) );

	foreach ( $scripts as $sk => $sv ) {
		wp_register_script( $sk, sprintf( $sv['file'], trailingslashit( $sv['url'] ), $min ), $sv['deps'], $version, $sv['footer'] );
	}
}

/**
 * Enqueue the Javascripts and Stylesheets.
 *
 * @since 1.0.0
 */
function clustertheme_enqueue_cssjs() {
	if ( clusterpress_is_email_preview() ) {
		return;
	}

	if ( clustertheme_is_maintenance() && ! current_user_can( 'manage_options' ) ) {
		wp_enqueue_style( 'clustertheme-maintenance' );

	} elseif ( is_front_page() && ! is_home() ) {
		wp_enqueue_style( 'clustertheme-onepage' );

		wp_enqueue_script( 'clustertheme-onepage' );
		wp_add_inline_script( 'clustertheme-onepage', '
			( function($) {
				$( \'#main\' ).onepage_scroll( {
					sectionContainer: "article",
					responsiveFallback: 600,
					loop: false
				} );

				$( \'#main article \').on( \'click\', \'a\', function( e ) {
					if ( $( \'body\' ).hasClass( \'disabled-onepage-scroll\' ) ) {
						return e;
					}

					var hash = $( e.currentTarget ).context.hash || \'\', scrollIndex;

					if ( hash ) {
						scrollIndex = Number( 0 + hash.replace( \'#panel\', \'\' ) );
					}

					if ( ! scrollIndex ) {
						return e;
					}

					e.preventDefault();

					$( \'#main\' ).moveTo( scrollIndex );
				} );
			} )( jQuery );
		' );
	} else {
		if ( ( ! is_page() && ! is_single() && ! is_404() ) || apply_filters( 'clustertheme_use_sidebar', false ) ) {
			wp_enqueue_style( 'clustertheme-layout' );
		} else {
			wp_enqueue_style( 'clustertheme-main' );
		}

		wp_enqueue_script( 'clustertheme-navigation' );
		wp_enqueue_script( 'clustertheme-skip-link-focus-fix' );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	$colorscheme = get_theme_mod( 'colorscheme', 'default' );

	// Load the selected colorscheme.
	if ( 'default' !== $colorscheme ) {
		wp_enqueue_style( sprintf( 'clustertheme-%s', sanitize_file_name( $colorscheme ) ) );
	}

	do_action( 'clustertheme_enqueue_cssjs' );
}

/**
 * Get the current ClusterTheme locale
 *
 * @since 1.0.0.
 *
 * @return string the Locale using the 2 first chars.
 */
function clustertheme_locale() {
	$lang = get_locale();

	if ( ! $lang ) {
		$lang = 'en_US';
	}

	return substr( $lang, 0, 2 );
}

/**
 * Get the available locales.
 *
 * @since 1.0.0
 *
 * @retun array The list of supported locales.
 */
function clustertheme_get_available_locale() {
	return array_merge( get_available_languages(), array( 'en_US' ) );
}

/**
 * Get nav menus to register for the site.
 *
 * @since 1.0.0
 *
 * @param  bool $is_main_site Whether the current site is the main site or not.
 * @return array              The list of nav menus to register.
 */
function clustertheme_get_nav_menus( $is_main_site = false ) {
	// Available nav menus
	$nav_menus = array(
		'header-menu-fr' => esc_html__( 'Menu d\'entête (fr)', 'clustertheme' ),
		'header-menu-en' => esc_html__( 'Menu d\'entête (en)', 'clustertheme' ),
	);

	// When not on main site, only register the one for the site locale.
	if ( ! $is_main_site ) {
		$nav_menu_key = sprintf( 'header-menu-%s', clustertheme_locale() );
		$nav_menus    = array_intersect_key( $nav_menus, array( $nav_menu_key => true ) );
	}

	$nav_menus['footer-menu'] = esc_html__( 'Menu des profils externes', 'clustertheme' );

	return apply_filters( 'clustertheme_get_nav_menus', $nav_menus );
}

/**
 * Get the nav menu arguments according to the current locale.
 *
 * @since 1.0.0
 *
 * @return array The Nav Menu arguments.
 */
function clustertheme_get_nav_args() {
	$lang = clustertheme_locale();

	return apply_filters( 'clustertheme_get_nav_args', array(
		'theme_location' => sprintf( 'header-menu-%s', esc_attr( $lang ) ),
		'menu_id'        => 'header-menu'
	) );
}

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 *
 * @since 1.0.0
 */
function clustertheme_widgets_init() {
	// Available sidebars.
	$sidebars = array(
		'fr' => array(
			'name'          => esc_html__( 'Sidebar (fr)', 'clustertheme' ),
			'id'            => 'sidebar-fr',
			'description'   => esc_html__( 'Ajoutez vos widgets pour les visiteurs français ici.', 'clustertheme' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		),
			'en' => array(
			'name'          => esc_html__( 'Sidebar (en)', 'clustertheme' ),
			'id'            => 'sidebar-en',
			'description'   => esc_html__( 'Ajoutez vos widgets pour les visiteurs anglais ici.', 'clustertheme' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		),
	);

	// When not on main site, only register the one for the site locale.
	if ( ! clustertheme_is_main_site() ) {
		$sidebars = array_intersect_key( $sidebars, array( clustertheme_locale() => true ) );
	}

	// Register the dynamic sidebars
	foreach ( $sidebars as $sidebar ) {
		register_sidebar( $sidebar );
	}
}

/**
 * Adds custom classes to the array of body classes.
 *
 * @since 1.0.0
 *
 * @param  array $classes Classes for the body element (required).
 * @return array          Unique body classes.
 */
function clustertheme_body_classes( $classes ) {
	$is_maintenance_on = clustertheme_is_maintenance();

	// Site is in maintenance, simply return the corresponding class.
	if ( $is_maintenance_on && ! current_user_can( 'manage_options' ) ) {
		return array( 'maintenance' );
	}

	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Add class on front page.
	if ( is_front_page() && 'posts' !== get_option( 'show_on_front' ) ) {
		$classes[] = 'clustertheme-front-page';
	}

	// Add a class to inform admin the Site is in maintenance mode.
	if ( $is_maintenance_on ) {
		$classes[] = 'clustertheme-maintenance';
	}

	return array_unique( $classes );
}

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function clustertheme_extra_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', bloginfo( 'pingback_url' ), '">';
	}

	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

/**
 * Append the static front page children as scrollable panels.
 *
 * @since 1.0.0
 *
 * @param  array    $posts The list of posts.
 * @param  WP_Query $q     The WordPress query
 * @return array          The list of panels.
 */
function clustertheme_append_front_page_panels( $posts, $q ) {
	$parent = $q->get( 'page_id' );

	if ( ! $parent || (int) $parent !== (int) get_option( 'page_on_front' ) || 'posts' === get_option( 'show_on_front' ) ) {
		return $posts;
	}

	if ( clustertheme_is_maintenance() && ! current_user_can( 'manage_options' ) ) {
		return $posts;
	}

	$children = get_pages( array( 'parent' => $parent, 'sort_column' => 'menu_order' ) );

	if ( ! $children ) {
		return $posts;
	} else {
		$posts = array_merge( $posts, $children );
	}

	return $posts;
}

/**
 * Catch the thumbnail credit for a later use in the single post template.
 *
 * @since 1.0.0
 *
 * @param  array   $attr       The thumbnail attributes
 * @param  WP_Post $attachment The attachment object.
 * @return array               Unchanged attributes.
 */
function clustertheme_get_thumbnail_credit( $attr, WP_Post $attachment ) {
	if ( ! empty( $attachment->post_content ) ) {
		clustertheme()->thumbnail_overlay = $attachment->post_content;
	}

	return $attr;
}

/**
 * Check if the excerpt was trimed.
 *
 * @since 1.0.0
 *
 * @param  string $text The excerpt text.
 * @return string       The excerpt text.
 */
function clustertheme_excerpt_was_trimed( $text ) {
	if ( false !== strpos( $text, 'class="readmore"' ) ) {
		clustertheme()->excerpt_was_trimed = true;
	}

	return $text;
}

/**
 * Override the excerpt length.
 *
 * @since 1.0.0
 *
 * @param  int $length The excerpt length.
 * @return int         The excerpt length.
 */
function clustertheme_excerpt_length( $length = 0 ) {
	return 50;
}

/**
 * Override the read more link
 *
 * @since 1.0.0
 *
 * @return string The read more link.
 */
function clustertheme_excerpt_more() {
	$post_id = get_the_ID();

	$link = sprintf( '<a href="%1$s" class="readmore">%2$s &rarr;</a>',
		esc_url( get_permalink( $post_id ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Poursuivre la lecture<span class="screen-reader-text"> "%s"</span>', 'clustertheme' ), get_the_title( $post_id ) )
	);

	return apply_filters( 'clustertheme_excerpt_more', ' &hellip; ' . $link );
}

/**
 * Gist embed handler callback.
 *
 * @since 1.0.0
 *
 * @param array  $matches The RegEx matches from the provided regex when calling
 *                        wp_embed_register_handler().
 * @param array  $attr    Embed attributes.
 * @param string $url     The original URL that was matched by the regex.
 * @param array  $rawattr The original unmodified attributes.
 * @return string The embed HTML.
 */
function clustertheme_gist_handler( $matches, $attr, $url, $rawattr ) {
	// Only display gists on single pages
	if ( is_front_page() || is_home() ) {
		return;
	}

	if ( isset( $matches[3] ) ) {
		$url = $matches[1] . '.js';

		if ( isset( $matches[6] ) ) {
			$url = add_query_arg( 'file', preg_replace( '/[\-\.]([a-z]+)$/', '.\1', $matches[6] ), $url );
		}

		return sprintf( '<script src="%s"></script>', esc_url( $url ) );
	}
}

/**
 * Add a specific class to comments awaiting moderation.
 *
 * @since 1.0.0
 *
 * @param  array        $classes     The comment classes in an array.
 * @param  string|array $class       Optional. One or more classes to add to the class list.
 * @param  int          $comment_id  Comment ID.
 * @param  WP_Comment   $comment     WP_Comment object. Default current comment.
 * @return array                     The comment classes in an array.
 */
function clustertheme_comment_class( $classes = array(), $class = '', $comment_id = 0, $comment = null ) {
	if ( isset( $comment->comment_approved ) && 0 === (int) $comment->comment_approved ) {
		$classes[] = 'awaiting-moderation';
	}

	return $classes;
}

/**
 * Override the Edit comment link to include a dashicon.
 *
 * @since 1.0.0
 *
 * @param  string  $comment_link The edit comment link.
 * @param  int     $comment_id   The comment ID.
 * @param  sting   $comment      The edit comment text.
 * @return string                The edit comment link.
 */
function clustertheme_edit_comment_link( $comment_link = '', $comment_id = 0, $link_text = '' ) {
	return str_replace( $link_text, '<span class="dashicons dashicons-edit"></span>' . $link_text, $comment_link );
}

/**
 * Override the reply link arguments.
 *
 * @since 1.0.0
 *
 * @param  array  $comment_args The reply link arguments.
 * @return array                The reply link arguments.
 */
function clustertheme_comment_reply_link_args( $comment_args = array() ) {
	if ( empty( $comment_args['reply_text'] ) ) {
		$comment_args['reply_text'] = __( 'Répondre', 'clustertheme' );
	}

	return array_merge( $comment_args, array(
		'reply_text' => '<span class="dashicons dashicons-admin-comments"></span>' . $comment_args['reply_text'],
	) );
}

/**
 * Get the posts/post navigation arguments.
 *
 * @since 1.0.0
 *
 * @param  string $type The type of navigation arguments to get.
 * @return array        The navigation args.
 */
function clustertheme_navigation_args( $type = 'posts' ) {
	$navigation_args = array(
		'posts' => array(
			'prev_text' => sprintf( '<span class="dashicons dashicons-controls-play"></span> <span class="prev-text">%s</span>', esc_html__( 'Articles plus anciens', 'clustertheme' ) ),
			'next_text' => sprintf( '<span class="next-text">%s</span><span class="dashicons dashicons-controls-play"></span>', esc_html__( 'Articles plus récents', 'clustertheme' ) ),
		),
		'post' => array(
			'prev_text' => '<span class="dashicons dashicons-controls-play"></span><span class="prev-text">%title</span>',
			'next_text' => '<span class="next-text">%title</span><span class="dashicons dashicons-controls-play"></span>',
		),
	);

	$args = array();

	if ( isset( $navigation_args[$type] ) ) {
		$args = $navigation_args[$type];
	}

	return $args;
}

/**
 * Check if a landing page is available to override the static front page
 * for english visitors.
 *
 * @since 1.0.0
 *
 * @param  bool|int $retval A page ID to shortcircuit the option. False otherwise.
 * @return bool|int         A page ID to shortcircuit the option. False otherwise.
 */
function clustertheme_page_on_front( $retval = false ) {
	$landing_page = get_theme_mod( 'landing_page', 0 );

	if ( ! empty( $landing_page ) ) {
		$retval = (int) $landing_page;
	}

	return $retval;
}

/**
 * Check if a tagline is available to override the site description
 * for english visitors.
 *
 * @since 1.0.0
 *
 * @param  bool|string $retval The text for the tagline to shortcircuit the option. False otherwise.
 * @return bool|string         The text for the tagline to shortcircuit the option. False otherwise.
 */
function clustertheme_blogdescription( $retval = '' ) {
	$tagline = get_theme_mod( 'tagline', '' );

	if ( ! empty( $tagline ) ) {
		$retval = $tagline;
	}

	return $retval;
}

/**
 * Override the site description before display to markup bold elements if needed.
 *
 * @since 1.0.0
 *
 * @param  string $output The option value (site description).
 * @param  string $show   The option key(description).
 * @return string         The site description.
 */
function clustertheme_setup_description( $output = '', $show = '' ) {
	if ( 'description' !== $show || doing_action( 'wp_head' ) ) {
		return $output;
	}

	preg_match_all( '/\*(.*?)\*/', $output, $matches );

	if ( ! empty( $matches[1] ) ) {
		foreach ( $matches[1] as $key => $text ) {
			$output = str_replace( $matches[0][ $key ], '<strong>' . $text . '</strong>', $output );
		}
	}

	return $output;
}

/**
 * Make sure there's a version of the site icon for the login logo
 *
 * @since 1.0.0
 *
 * @param  array $icon_sizes The list of allowed icon sizes in Pixels.
 * @return array             The list of allowed icon sizes in Pixels.
 */
function clustertheme_login_logo_size( $icon_sizes = array() ) {
	return array_merge( $icon_sizes, array( 84 ) );
}

/**
 * Check if the sharing cards meta tags should be output.
 *
 * @since 1.0.0
 *
 * @return bool True if sharing cards meta tags should be output. False otherwise.
 */
function clustertheme_print_sharing_cards() {
	return apply_filters( 'clustertheme_print_sharing_cards', is_singular() );
}

/**
 * Use a template to render emails
 *
 * @since  1.0.0
 *
 * @param  string       $text The text of the email.
 * @return string|false       The html text for the email, or false.
 */
function clustertheme_set_html_content( $text = '' ) {
	if ( empty( $text ) ) {
		return false;
	}

	ob_start();
	get_template_part( 'email' );
	$email_template = ob_get_clean();

	if ( empty( $email_template ) ) {
		return false;
	}

	// Make sure the link to set or reset the password
	// will be clickable in text/html
	if ( did_action( 'retrieve_password_key' ) ) {
		preg_match( '/<(.+?)>/', $text, $match );

		if ( ! empty( $match[1] ) ) {

			$login_url = wp_login_url();
			$link      = "\n" . '<a href="' . $match[1] . '">' . $login_url . '</a>';

			if ( preg_match( '/[^<]' . addcslashes( $login_url, '/' ) . '/', $text ) ) {
				$text = preg_replace( '/[^<]' . addcslashes( $login_url, '/' ) . '/', $link, $text );
			} else {
				$text .= $link;
			}

			$text = str_replace( $match[0], '', $text );
		}
	}

	$pagetitle = esc_attr( get_bloginfo( 'name', 'display' ) );
	$content   = apply_filters( 'the_content', $text );

	// Make links clickable
	$content = make_clickable( $content );

	$email = str_replace( '{{pagetitle}}', $pagetitle,     $email_template );
	$email = str_replace( '{{content}}',   $content,       $email          );

	return $email;
}

/**
 * Use a multipart/alternate email.
 *
 * NB: follow the progress made on
 * https://core.trac.wordpress.org/ticket/15448
 *
 * @since 1.0.0
 *
 * @param PHPMailer $phpmailer The Mailer class.
 */
function clustertheme_email( PHPMailer $phpmailer ) {
	if ( empty( $phpmailer->Body ) ) {
		return;
	}

	$html_content = clustertheme_set_html_content( $phpmailer->Body );

	if ( $html_content ) {
		$phpmailer->AltBody = $phpmailer->Body;
		$phpmailer->Body    = $html_content;
	}
}

/**
 * Register the ClusterTheme Email post type.
 *
 * This is used to preview the Email template within the customizer.
 *
 * @since  1.0.0
 */
function clustertheme_register_email_type() {
	register_post_type(
		'clustertheme_email',
		array(
			'label'              => 'clustertheme_email',
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => false,
			'show_in_menu'       => false,
			'show_in_nav_menus'  => false,
			'query_var'          => false,
			'rewrite'            => false,
			'has_archive'        => false,
			'hierarchical'       => true,
		)
	);
}

/**
 * Get the hex color for the active color scheme.
 *
 * @since  1.0.0
 *
 * @return string the hex color for the active color scheme.
 */
function clustertheme_get_scheme_hex_color() {
	$hex = array(
		'default' => '#8e3d58',
		'dark'    => '#615175',
		'red'     => '#af1E2d',
	);

	$scheme = get_theme_mod( 'colorscheme', 'default' );

	if ( isset( $hex[ $scheme ] ) ) {
		return $hex[ $scheme ];
	}

	return $hex['default'];
}

/**
 * Display ClusterTheme font icons in external profile links menu.
 *
 * @since  1.0.0
 *
 * @param  string  $output The menu item output.
 * @param  WP_Post $item   Menu item object.
 * @param  int     $depth  Depth of the menu.
 * @param  array   $args   wp_nav_menu() arguments.
 * @return string          The menu item output with ClusterTheme icon.
 */
function clustertheme_nav_menu_attributes( $output = array(), $item = null, $depth = 1, $args = null ) {
	if ( empty( $args->theme_location ) || 'footer-menu' !== $args->theme_location || empty( $item->url ) ) {
		return $output;
	}

	$external_service   = parse_url( $item->url, PHP_URL_HOST );
	$icons              = apply_filters( 'clustertheme_profile_icons', array(
		'clustertheme-icons-twitter' => 'twitter.com',
		'clustertheme-icons-gitlab'  => 'gitlab.com',
		'clustertheme-icons-github'  => 'github.com',
		'clustertheme-icons-cluster' => 'cluster.press',
		'clustertheme-icons-paypal'  => 'paypal.com',
	) );

	$found = array_search( $external_service, $icons );

	if ( $found ) {
		$output = str_replace( $args->link_after, sprintf( '</span><span class="clustertheme-icons %s">', $found ), $output );
	} else {
		$output = str_replace( $args->link_after, sprintf( '</span>%s', esc_html( $item->post_title ) ), $output );
	}

	return $output;
}

/**
 * Alter the WP Query to only contain a ClusterTheme utility post.
 *
 * @since  1.0.0
 *
 * @param  WP_Query      $wq     The WP Query object.
 * @param  array         $post   Post properties in a keyed array.
 * @param  array         $args   Conditional tags in a keyed array.
 * @return null|WP_Query         Null if no unregistered post type were provided.
 *                               The WP_Query containing a ClusterTheme Post otherwise.
 */
function clustertheme_alter_posts_query( WP_Query $wq, $post = array(), $args = array() ) {
	if ( empty( $post['post_type'] ) || false !== array_search( $post['post_type'], get_post_types() ) ) {
		return $wq->clustertheme_null;
	}

	$post = (object) wp_parse_args( $post, array(
		'ID'             => 0,
		'comment_status' => 'closed',
		'comment_count'  => 0,
	) );

	// Set the queried object to avoid notices
	$wq->queried_object    = get_post( $post );
	$wq->queried_object_id = $wq->queried_object->ID;

	// Set the Posts list to be limited to our custom post.
	$posts = array( $post );

	// Reset some WP Query properties
	$wq->found_posts   = 1;
	$wq->max_num_pages = 1;
	$wq->posts         = $posts;
	$wq->post          = $post;
	$wq->post_count    = 1;

	$conditional_tags = wp_parse_args( $args, array(
		'is_home'       => false,
		'is_front_page' => false,
		'is_page'       => true,
		'is_single'     => false,
		'is_archive'    => false,
		'is_tax'        => false,
	) );

	foreach ( $conditional_tags as $key => $conditional_tag ) {
		$wq->{$key} = (bool) $conditional_tag;
	}

	return $wq;
}

/**
 * Shortcode to output the external profile links in post content.
 *
 * @since 1.0.0
 *
 * @return string HTML Output for the external profiles WP Nav Menu.
 */
function clustertheme_profiles_shortcode() {
	return sprintf( '<nav class="profiles-navigation">%s</nav>',
		clustertheme_external_profiles( false )
	);
}

/**
 * Build and return a dashicon or a clustertheme icon.
 *
 * @since  1.0.0
 *
 * @param  array|string $atts {
 *    The shortcode attribute.
 *    @type string $icon    The name of the icon to use.
 *                          Required.
 * }
 * @return string HTML Output.
 */
function clustertheme_icons_shortcode( $atts = array() ) {
	$a = shortcode_atts( array(
		'icon' => '',
	), $atts, 'clustertheme_icons_shortcode' );

	if ( empty( $a['icon'] ) ) {
		return '';
	}

	$type = explode( '-', $a['icon'] );

	if ( empty( $type[0] ) || false === array_search( $type[0], array( 'dashicons', 'clustertheme' ) ) ) {
		return '';
	}

	$classes = array( 'dashicons' );
	if ( 'clustertheme' === $type[0] ) {
		$classes = array( 'clustertheme-icons' );
	}

	$classes[] = $a['icon'];

	return sprintf( '<span class="%s"></span>',
		join( ' ', array_map( 'sanitize_html_class', $classes ) )
	);
}

/**
 * Shortcode to output the home page user action links.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function clustertheme_user_actions_shortcode() {
	$actions = array();

	if ( ! is_user_logged_in() ) {
		if ( true === (bool) get_option( 'users_can_register', false ) ) {
			$actions['register'] = array(
				'link'     => wp_registration_url(),
				'text'     => __( 'Rejoindre', 'clustertheme' ),
				'classes' => array( 'dashicons', 'dashicons-nametag' ),
			);
		}

		$actions['login'] = array(
			'link'     => wp_login_url(),
			'text'     => __( 'Connexion', 'clustertheme' ),
			'classes'  => array( 'dashicons', 'dashicons-admin-network' ),
		);

		if ( 'fr' === clustertheme_locale() ) {
			$flag   = '🇬🇧';
			$locale = 'en_US';
			$text   = 'English';
		} else {
			$flag   = '🇫🇷';
			$locale = 'fr_FR';
			$text   = 'Français';
		}

		$actions['switch_locale'] = array(
			'link'     => add_query_arg( 'locale', $locale ),
			'text'     => $text,
			'emoji'    => $flag,
		);

	} else {
		$actions['logout'] = array(
			'link'     => wp_logout_url(),
			'text'     => __( 'Déconnexion', 'clustertheme' ),
			'classes' => array( 'dashicons', 'dashicons-migrate' ),
		);

		$actions['user'] = array(
			'link'     => CT_User_Nav_Widget::get_user_url(),
			'text'     => __( 'Mon Profil', 'clustertheme' ),
			'classes' => array( 'dashicons', 'dashicons-admin-users' ),
		);
	}

	$actions = apply_filters( 'clustertheme_user_actions_shortcode', $actions );

	if ( empty( $actions ) ) {
		return '';
	}

	$output = '<ul class="group-buttons square">';

	foreach ( $actions as $action ) {
		$dashicon = '';
		if ( ! empty( $action['classes'] ) )  {
			$dashicon = sprintf( '<span class="%s"></span>',
				join( ' ', array_map( 'sanitize_html_class', $action['classes'] ) )
			);
		}

		$emoji = '';
		if ( ! empty( $action['emoji'] ) )  {
			$emoji = '<span class="flag">' . $action['emoji'] . '</span>';
		}

		$output .= sprintf( '
			<li>
				<a href="%1$s">
					%2$s%3$s
					<span class="text">%4$s</span>
				</a>
			</li>
		', $action['link'], $dashicon, $emoji, $action['text'] );
	}

	return $output . '</ul>';
}
