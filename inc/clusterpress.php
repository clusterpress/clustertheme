<?php
/**
 * ClusterPress functions
 *
 * @package ClusterTheme\inc
 * @subpackage clusterpress
 *
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Switch ClusterPress locale
 *
 * NB: this probably should be in the ClusterPress plugin.
 *
 * @since 1.0.0
 *
 * @param string $locale The switched locale.
 */
function clustertheme_switch_locale( $locale = '' ) {
	$cp = clusterpress();

	// Load ClusterPress textdomain.
	cp_load_textdomain();

	// Get all external clusters.
	$external_clusters = array_diff( $cp->active_clusters, array(
		'core',
		'user',
		'interactions',
		'site',
	) );

	if ( $external_clusters ) {
		foreach ( $external_clusters as $cluster ) {
			if ( ! method_exists( $cp->{$cluster}, 'load_languages' ) ) {
				continue;
			}

			$cp->{$cluster}->load_languages();
		}
	}
}
add_action( 'switch_locale', 'clustertheme_switch_locale' );

/**
 * Register specific ClusterPress sidebar for widgets.
 *
 * @since 1.0.0
 */
function clustertheme_cp_register_sidebars() {
	if ( ! cp_is_main_site() ) {
		return;
	}

	// ClusterPress Sidebars
	register_sidebar( array(
		'name'          => esc_html__( 'ClusterPress Sidebar (fr)', 'clustertheme' ),
		'id'            => 'sidebar-clusterpress-fr',
		'description'   => esc_html__( 'Ajoutez vos widgets pour les pages ClusterPress des visiteurs français ici.', 'clustertheme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'ClusterPress Sidebar (en)', 'clustertheme' ),
		'id'            => 'sidebar-clusterpress-en',
		'description'   => esc_html__( 'Ajoutez vos widgets pour les pages ClusterPress des visiteurs anglais ici.', 'clustertheme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'clustertheme_cp_register_sidebars', 15 );

/**
 * Use the layout containing the sidebar for ClusterPress area.
 *
 * @since 1.0.0
 *
 * @param  bool $return The original layout preference.
 * @return bool        The layout preference.
 */
function clustertheme_cp_use_sidebar( $return = false ) {
	if ( false === $return ) {
		$return = is_clusterpress() && ! is_404();
	}

	return $return;
}
add_filter( 'clustertheme_use_sidebar', 'clustertheme_cp_use_sidebar' );

/**
 * Remove the likes shortcode from post content.
 *
 * @since 1.0.0
 *
 * @param  string $content The post content.
 * @return string          The post content.
 */
function clustertheme_strip_likes_from_content( $content = '' ) {
	if ( ! cp_cluster_is_enabled( 'interactions' ) || ! cp_interactions_is_like_enabled() ) {
		return $content;
	}

	return str_replace( '[clusterpress_likes_button]', '', $content );
}
add_filter( 'the_content', 'clustertheme_strip_likes_from_content', 6, 1 );

/**
 * Add the likes shortcode to post footer.
 *
 * @since 1.0.0
 *
 * @param  array $buttons The buttons list.
 * @return array          The buttons list.
 */
function clustertheme_likes_button( $buttons = array() ) {
	if ( ! cp_cluster_is_enabled( 'interactions' ) || ! cp_interactions_is_like_enabled() ) {
		return $buttons;
	}

	$like_button = do_shortcode( '[clusterpress_likes_button]' );

	if ( $like_button ) {
		$buttons[] = $like_button;
	}

	return $buttons;
}
add_filter( 'clustertheme_extra_entry_footer', 'clustertheme_likes_button', 10, 1 );

/**
 * Use specific Widget titles for some ClusterPress widgets on the main site.
 *
 * @since 1.0.0
 *
 * @param  string $title    The widget title.
 * @param  array  $instance The widget instance.
 * @param  string $id_base  The widget base ID.
 * @return string           The widget title.
 */
function clustertheme_set_widget_title( $title = '', $instance = array(), $id_base = '' ) {
	if ( ! cp_is_main_site() || 'en' !== clustertheme_locale() || ! $id_base ) {
		return $title;
	}

	switch ( $id_base ) {
		case 'cp_interactions_liked_posts_widget' :
			$title = 'Most Liked Plugins';
			break;

		case 'cp_site_category_widget' :
			$title = 'Site categories';
			break;
	}

	return $title;
}
add_filter( 'widget_title', 'clustertheme_set_widget_title', 10, 3 );

/**
 * Adjust the Archive page title for clusters having an archive page
 *
 * @since  1.0.0
 *
 * @param  WP_Query|null $q The WordPress main query.
 */
function clustertheme_cp_archive_title( WP_Query $q = null ) {
	if ( empty( $q ) || ! cp_is_main_site() || 'en' !== clustertheme_locale() ) {
		return;
	}

	$archive = $q->get( 'cp_network' );
	$cp      = clusterpress();

	switch( $archive ) {
		case cp_users_get_slug() :
			$q->set( 'cp_post_title', esc_html__( 'Membres', 'clustertheme' ) );
			$cp->user->archive_title = $q->get( 'cp_post_title' );

			break;

		case cp_sites_get_slug() :
			$q->set( 'cp_post_title', esc_html__( 'Sites', 'clustertheme' ) );
			$cp->site->archive_title = $q->get( 'cp_post_title' );

			break;

		case 'extensions' :
			if ( ! cp_cluster_is_enabled( 'plugins' ) ) {
				break;
			}

			$q->set( 'cp_post_title', esc_html__( 'Extensions', 'clustertheme' ) );
			$cp->plugins->archive_title = $q->get( 'cp_post_title' );

			break;
	}
}
add_action( 'cp_user_parse_query',    'clustertheme_cp_archive_title' );
add_action( 'cp_site_parse_query',    'clustertheme_cp_archive_title' );
add_action( 'cp_plugins_parse_query', 'clustertheme_cp_archive_title' );

/**
 * Make sure the shortcode used by the ClusterPress plugins addon
 * will use the right locale.
 *
 * @since  1.0.0
 */
function clustertheme_cp_reset_plugins_locale() {
	if ( ! is_locale_switched() ) {
		return;
	}

	// We also need to change the locale
	clusterpress()->plugins->locale = get_locale();
}
add_action( 'cp_plugins_parse_query', 'clustertheme_cp_reset_plugins_locale', 1 );

/**
 * Are we viewing a single ClusterPress object ?
 *
 * @since 1.0.0
 *
 * @param bool $return True if viewing a single ClusterPress object. False otherwise.
 */
function clustertheme_is_cp_single( $return = false ) {
	$cp = clusterpress();
	$ct = clustertheme();

	if ( is_clusterpress() && ! empty( $cp->cluster->displayed_object ) ) {
		$ct->sharing_card = $cp->cluster->displayed_object;
		$return = true;
	}

	return $return;
}
add_filter( 'clustertheme_print_sharing_cards', 'clustertheme_is_cp_single', 10, 1 );

/**
 * Adapt sharing cards meta information to ClusterPress displayed object.
 *
 * @since 1.0.0
 *
 * @param array $metas A list of Twitter/Facebook parameters.
 * @return             A list of Twitter/Facebook parameters.
 */
function clustertheme_cp_sharing_cards( $metas = array() ) {
	$ct = clustertheme();

	if ( empty( $ct->sharing_card ) ) {
		return $metas;
	}

	if ( 'public' !== cp_get_network_type() ) {
		return array( 'twitter:title' => false );
	}

	$metas['twitter:card'] = 'summary';

	// It's a user!
	if ( cp_is_user() ) {
		$description = cp_displayed_user()->user_description;

		if ( empty( $description ) ) {
			$description = sprintf( __( '%s est un membre de la tribu ClusterPress ! Rejoignez le et contribuer aux projets open source ClusterPress.', 'clustertheme' ), cp_displayed_user()->user_nicename );
		}

		$metas['twitter:image'] = esc_url_raw( get_avatar_url( cp_displayed_user()->user_email, array( 'size' => 120 ) ) );

	// It's a site!
	} elseif ( cp_is_site() ) {
		$needs_switch = ! cp_is_main_site( cp_get_displayed_site_id() );

		if ( $needs_switch ) {
			switch_to_blog( cp_get_displayed_site_id() );
		}

		$description            = get_bloginfo( 'description' );
		$metas['twitter:image'] = esc_url_raw( get_site_icon_url( 120 ) );

		if ( $needs_switch ) {
			restore_current_blog();
		}

	// It's a plugin!
	} elseif ( cp_cluster_is_enabled( 'plugins' ) && cpp_is_extension() ) {
		$description            = cpp_do_lang_shortcode( $ct->sharing_card->post_content );
		$metas['twitter:image'] = esc_url_raw( get_the_post_thumbnail_url( $ct->sharing_card, array( 120, 120 ) ) );

	} else {
		return $metas;
	}

	if ( ! empty( $metas['twitter:image'] ) ) {
		$metas['og:image'] = $metas['twitter:image'];
	}

	if ( ! empty( $description ) ) {
		$metas['twitter:description'] = wp_strip_all_tags( apply_filters( 'the_excerpt', wp_trim_words( $description, 40, '...' ) ) );
		$metas['og:description']      = $metas['twitter:description'];
	}

	if ( empty( $metas['og:url'] ) ) {
		$metas['og:url'] = esc_url_raw( site_url( $_SERVER['REQUEST_URI'] ) );
	}

	return $metas;
}
add_filter( 'clustertheme_sharing_cards', 'clustertheme_cp_sharing_cards', 10, 1 );

/**
 * Get contatc methods according to their properties
 *
 * @since 1.0.0
 *
 * @param array  $args A list of parameters to request for.
 * @return array       The list of contact methods and their properties.
 */
function clustertheme_get_contact_methods( $args = array() ) {
	$r = wp_parse_args( $args, array(
		'property' => false,
		'signup'   => null,
	) );

	$contact_methods = apply_filters( 'clustertheme_get_contact_methods', array(
		'twitter' => array(
			'text'        => __( 'Compte Twitter', 'clustertheme' ),
			'signup'      => false,
			'id'          => 'twitter',
			'description' => '',
			'base_url'    => 'https://twitter.com/',
			'dashicon'    => 'clustertheme-icons clustertheme-icons-twitter',
		),
		'gitlab'  => array(
			'text'        => __( 'Compte GitLab', 'clustertheme' ),
			'signup'      => true,
			'id'          => 'gitlab',
			'description' => __( 'Optionnel. Les sources des projets de ClusterPress sont hébergés sur Gitlab. Ajoutez votre nom d\'utilisateur sur Gitlab.com si vous souhaitez y contribuer.', 'clustertheme' ),
			'base_url'    => 'https://gitlab.com/',
			'dashicon'    => 'clustertheme-icons clustertheme-icons-gitlab',
		),
		'github'  => array(
			'text'        => __( 'Compte Github', 'clustertheme' ),
			'signup'      => false,
			'id'          => 'github',
			'description' => '',
			'base_url'    => 'https://github.com/',
			'dashicon'    => 'clustertheme-icons clustertheme-icons-github',
		),
	) );

	if ( is_null( $r['signup'] ) ) {
		if ( false === $r['property'] ) {
			return $contact_methods;
		}

		return wp_list_pluck( $contact_methods, $r['property'], 'id' );
	} else {
		return wp_list_filter( $contact_methods, array( 'signup' => (bool) $r['signup'] ), 'AND' );
	}
}

/**
 * Add contact methods to existing ones.
 *
 * @since 1.0.0
 *
 * @param array $contact_methodes existing contact methods.
 * @return array                  The contact methods.
 */
function clustertheme_add_contact_methods( $contact_methods = array() ) {
	return array_merge( $contact_methods, clustertheme_get_contact_methods( array( 'property' => 'text' ) ) );
}
add_filter( 'user_contactmethods', 'clustertheme_add_contact_methods', 10, 1 );

/**
 * Output/Return the contact method value.
 *
 * @since 1.0.0
 *
 * @param string $field_id The unique identifier for the Sharing service.
 * @return string          The external profile service link or the URL to the external profile service.
 */
function clustertheme_display_contact_method( $field_id = '', $field_value = '', $echo = true ) {
	if ( empty( $field_id ) || empty( $field_value ) ) {
		return;
	}

	$outputs_data = (object) clustertheme_get_contact_methods();

	if ( empty( $outputs_data->{$field_id} ) ) {
		return;
	}

	// In case people are using url instead of the username!
	$pattern = untrailingslashit( str_replace( 'https://', '', $outputs_data->{$field_id}['base_url'] ) );

	if ( preg_match( '/' . $pattern . '\/(.*?)$/', $field_value, $matches ) ) {
		if ( ! empty( $matches[1] ) ) {
			$field_value = $matches[1];
		} else {
			return;
		}
	}

	if ( false === $echo ) {
		return $outputs_data->{$field_id}['base_url'] . $field_value;
	}

	printf( '<a href="%1$s"><span class="%2$s"></span></a>',
		esc_url( $outputs_data->{$field_id}['base_url'] . $field_value ),
		esc_attr( $outputs_data->{$field_id}['dashicon'] )
	);
}

/**
 * Output extra fields into the WP Signups form.
 *
 * @since 1.0.0
 *
 * @param null|WP_Error $errors. Null or the WP Error object.
 */
function clustertheme_signup_fields( $errors = null ) {
	$signup_fields = clustertheme_get_contact_methods( array( 'signup' => true ) );

	if ( ! $signup_fields ) {
		return;
	}

	foreach ( $signup_fields as $field => $properties ) {

		$value = '';
		if ( ! empty( $_POST[ $field ] ) ) {
			$value = $_POST[ $field ];
		}

		$error = '';
		if ( is_a( $errors, 'WP_Error' ) && $errmsg = $errors->get_error_message( 'gitlab' ) ) {
			$error = '<p class="error">' . $errmsg . '</p>';
		}

		$description = '';
		if ( ! empty( $properties['description'] ) ) {
			$description = esc_html( $properties['description'] );
		}

		printf( '
				<label for="%1$s">%2$s</label>
				<input name="%1$s" type="text" id="%1$s" value="%3$s" maxlength="200" style="width: %4$s; font-size: 24px; margin: 5px 0px;" /><br />
				%5$s
				%6$s
			',
			esc_attr( $field ),
			esc_html( $properties['text'] ),
			esc_attr( $value ),
			'100%',
			$error,
			$description
		);
	}
}
add_action( 'clustertheme_before_locale_signup_field', 'clustertheme_signup_fields' );

/**
 * Extra signup validation rules.
 *
 * @since 1.0.0
 *
 * @param  array $result The validation results.
 * @return array         The validation results.
 */
function clustertheme_cp_validate_signup( $result = array() ) {
	$errors = $result['errors'];

	if ( ! is_wp_error( $errors ) || $errors->get_error_code() ) {
		return $result;
	}

	if ( ! empty( $_POST['gitlab'] ) ) {
		$gitlab_url = clustertheme_display_contact_method( 'gitlab', $_POST['gitlab'], false );

		$check_gitlab_account = wp_safe_remote_get( $gitlab_url, array(
			'timeout'    => 30,
			'user-agent' => 'ClusterPress signup (WordPress/' . get_bloginfo( 'version' ) . '); ' . home_url(),
		) );

		$content    = wp_remote_retrieve_body( $check_gitlab_account );
		$metas      = wp_kses( $content, array( 'meta' => array( 'content' => true, 'property' => true ) ) );
		$meta_array = array();

		if ( preg_match_all( '/<meta [^>]+>/', $metas, $matches ) ) {
			foreach ( $matches[0] as $value ) {
				if ( preg_match( '/content="([^"]+)" property="([^"]+)"/', $value, $new_matches ) ) {
					$meta_array[ $new_matches[2] ] = $new_matches[1];
				}
			}
		}

		if ( empty( $meta_array['og:url'] ) || $gitlab_url !== $meta_array['og:url'] ) {
			$result['errors']->add(
				'gitlab',
				__( 'Merci de vérifier votre compte GitLab, il semble inexistant sur Gitlab.', 'clustertheme' )
			);
		}
	}

	return $result;
}
add_filter( 'clustertheme_ms_validate_signup', 'clustertheme_cp_validate_signup', 15, 1 );

/**
 * Save ClusterPress specific metas for the user.
 *
 * @since 1.0.0
 *
 * @param array $metas The list of meta values for meta keys.
 */
function clustertheme_cp_signup_meta( $metas = array() ) {
	if ( empty( $metas['clustertheme'] ) ) {
		return $metas;
	}

	$signup_fields = clustertheme_get_contact_methods( array( 'signup' => true ) );
	if ( ! $signup_fields ) {
		return $metas;
	}

	foreach ( $signup_fields as $field ) {
		if ( empty( $_POST[ $field['id'] ] ) ) {
			continue;
		}

		$metas['clustertheme'][ $field['id'] ] =  sanitize_text_field( $_POST[ $field['id'] ] );
	}

	return $metas;
}
add_filter( 'add_signup_meta', 'clustertheme_cp_signup_meta', 15, 1 );

/**
 * Init the ClusterPress profile loop including contact methods only.
 *
 * @since 1.0.0
 *
 * @return bool True if the user has contact methods. False otherwise.
 */
function clustertheme_user_has_contact_methods() {
	return cp_user_has_fields( array(
		'include' => clustertheme_get_contact_methods( array(
			'property' => 'id',
		) ),
	) );
}

/**
 * Init the ClusterPress profile loop excluding contact methods.
 *
 * @since 1.0.0
 *
 * @return bool True if the user has profile fields. False otherwise.
 */
function clustertheme_user_has_fields() {
	return cp_user_has_fields( array(
		'exclude' => clustertheme_get_contact_methods( array(
			'property' => 'id',
		) ),
	) );
}

/**
 * Output the external profile link.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function clustertheme_user_the_contact_method() {
	return clustertheme_display_contact_method( cp_user_get_field_id(), cp_user_get_field_value() );
}

/**
 * Register the delete account screen.
 *
 * @since 1.0.0
 */
function clustertheme_register_user_section() {
	// Registers The User's profile Delete account sub-section
	cp_register_cluster_section( 'user', array(
		'id'            => 'delete-user-account',
		'name'          => __( 'Supprimer le compte.', 'clustertheme' ),
		'cluster_id'    => 'user',
		'slug'          => 'supprimer-compte',
		'rewrite_id'    => 'clustertheme_delete_account',
		'template'      => 'user/single/manage/delete-account',
		'template_dir'  => '',
		'toolbar_items' => array(
			'position'   => 100,
			'capability' => 'cp_edit_single_user',
			'parent'     => cp_user_get_manage_rewrite_id(),
		),
	) );
}
add_action( 'cp_register_cluster_sections', 'clustertheme_register_user_section', 15 );

/**
 * Save the locale switch if needed.
 *
 * @since 1.0.0
 *
 * @param WP_User $user the User object.
 */
function clustertheme_edit_user_locale( WP_User $user = null ) {
	if ( empty( $user->ID ) || empty( $_POST['locale'] ) ) {
		return;
	}

	// Validate locale
	$languages = array_flip( clustertheme_get_available_locale() );

	if ( ! isset( $languages[ $_POST['locale'] ] ) ) {
		return;
	}

	$user->locale = $_POST['locale'];
}
add_action( 'cp_user_before_edit_user', 'clustertheme_edit_user_locale', 10, 1 );

/**
 * Delete an account.
 *
 * @since 1.0.0
 */
function clustertheme_delete_account() {
	if ( empty( $_POST['cp_delete_account_submit'] ) ) {
		return;
	}

	if ( ! cp_user_is_self_profile() ) {
		cp_feedback_redirect( 'not-allowed', 'error' );
	}

	check_admin_referer( 'cp_delete_account' );

	if ( empty( $_POST['cp_delete_account']['confirm'] ) ) {
		cp_feedback_redirect( 'account-02', 'error' );
	}

	$user_id = cp_get_displayed_user_id();

	// Deal with multisite configs
	if ( is_multisite() ) {
		require_once( ABSPATH . '/wp-admin/includes/ms.php'   );
		require_once( ABSPATH . '/wp-admin/includes/user.php' );

		$retval = wpmu_delete_user( $user_id );

	// Or regular configs
	} else {
		require_once( ABSPATH . '/wp-admin/includes/user.php' );
		$retval = wp_delete_user( $user_id );
	}

	if ( ! $retval ) {
		cp_feedback_redirect( 'account-02', 'error' );
	}

	wp_redirect( site_url() );
	exit();
}
add_action( 'cp_page_actions', 'clustertheme_delete_account' );

/**
 * Fix the layout for ClusterPress single doc.
 *
 * @since  1.0.0
 *
 * @param  array  $classes The body classes in an array.
 * @return array           The body classes in an array.
 */
function clustertheme_cp_doc_body_class( $classes = array() ) {
	if ( is_page() && is_clusterpress() && ! cp_is_main_site() ) {
		if ( false !== array_search( 'cp-doc', $classes ) ) {
			$page_index = array_search( 'page', $classes );

			if ( ! empty( $page_index ) ) {
				unset( $classes[ $page_index ] );
			}
		}
	}

	return $classes;
}
add_filter( 'cp_set_body_class', 'clustertheme_cp_doc_body_class', 10, 1 );

/**
 * Override the profile url of the CT User Nav widget.
 *
 * @since 1.0.0
 *
 * @param  string  $profile_url The profile URL.
 * @param  WP_USer $user        The User object.
 * @return string               The profile URL.
 */
function clustertheme_cp_user_profile_url( $profile_url = '', $user = null ) {
	if ( empty( $user->ID ) ) {
		return $profile_url;
	}

	return cp_user_get_url( array(), $user );
}
add_filter( 'clustertheme_user_profile_url', 'clustertheme_cp_user_profile_url', 10, 2 );

/**
 * On Subsite, fetch the user's notifications.
 *
 * @since 1.0.0
 *
 * @param  array $navitems The CT User Nav Widet items.
 * @return array           The CT User Nav Widet items.
 */
function clustertheme_cp_user_nav_urls( $navitems = array() ) {
	if ( cp_is_main_site() || empty( $navitems['user'] ) ) {
		return $navitems;
	}

	$user = wp_get_current_user();
	$cp   = clusterpress();

	// Init the toolbars
	$cp->user->toolbars();

	// Set user nodes urls
	$user_nodes = $cp->user->toolbar->populate_urls( array(
		'object'          => $user,
		'type'            => 'WP_User',
		'parent_group'    => 'single-bar',
		'url_callback'    => 'cp_user_get_url',
		'filter'          => 'cp_user_cluster_get_toolbar_urls',
		'capability_args' => array( 'user_id' => $user->ID, 'toolbar' => 'user_bar' ),
	) );

	// Get primary nav
	$user_nodes = wp_list_filter( $user_nodes, array( 'parent' => false ) );

	// Sort nav.
	$user_nodes = $cp->user->toolbar->sort_items( $user_nodes );

	$subitems = array();

	foreach ( $user_nodes as $user_node ) {
		if ( empty( $user_node->count ) ) {
			continue;
		}

		$subitems[] = array(
			'link'    => $user_node->href,
			'text'    => $user_node->title,
			'classes' => array( 'dashicons', $user_node->dashicon ),
			'count'   => $user_node->count,
		);
	}

	if ( ! empty( $subitems ) ) {
		$navitems['user']['subitems'] = $subitems;
	}

	return $navitems;
}
add_filter( 'clustertheme_user_nav_urls', 'clustertheme_cp_user_nav_urls', 10, 1 );

/**
 * Add the Download ClusterPress action if extension is found.
 *
 * @since 1.0.0
 *
 * @param  array $actions The list of user actions.
 * @return array          The list of user actions.
 */
function clustertheme_cp_plugin_user_action( $actions = array() ) {
	if ( ! cp_cluster_is_enabled( 'plugins' ) ) {
		return $actions;
	}

	$clusterpress = get_posts( array(
		'name'      => 'clusterpress',
		'post_type' => 'cpp_extensions',
		'numberposts' => 1,
	) );

	if ( empty( $clusterpress ) ) {
		return $actions;
	}

	$clusterpress = get_post( reset( $clusterpress ) );

	if ( empty( $clusterpress ) ) {
		return $actions;
	}

	return array_merge( $actions, array( 'clusterpress' => array(
		'link'     => cpp_get_extension_link( array(
			'slug'       => cpp_get_releases_slug(),
			'rewrite_id' => cpp_get_releases_rewrite_id(),
		), $clusterpress ),
		'text'    => __( 'Télécharger', 'clustertheme' ),
		'classes' => array( 'clustertheme-icons', 'clustertheme-icons-cluster' ),
	) ) );
}
add_filter( 'clustertheme_user_actions_shortcode','clustertheme_cp_plugin_user_action', 10, 1 );
