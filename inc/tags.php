<?php
/**
 * ClusterPress template tags.
 *
 * @package ClusterTheme\inc
 * @subpackage tags
 *
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Get the front page panel index class.
 *
 * @since  1.0.0
 *
 * @return string the front page panel index.
 */
function clustertheme_front_page_panel_index() {
	$ct = clustertheme();

	if ( empty( $ct->front_page_panel_index ) ) {
		$ct->front_page_panel_index = 1;
	} else {
		$ct->front_page_panel_index += 1;
	}

	$classes = 'panel%s';
	if ( 0 === $ct->front_page_panel_index % 2 ) {
		$classes = 'panel%s dark';
	}

	return sprintf( $classes, $ct->front_page_panel_index );
}

/**
 * Display the site logo.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function clustertheme_custom_logo() {
	if ( ! has_custom_logo() ) {
		return;
	}
	?>
	<div id="site-logo">
		<?php the_custom_logo(); ?>
	</div>
	<?php
}

/**
 * Display the Site name.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function clustertheme_blogname() {
	echo clustertheme_get_blogname();
}

	/**
	 * Get the Site name
	 *
	 * @since 1.0.0
	 *
	 * @return  string The site name.
	 */
	function clustertheme_get_blogname() {
		$blog_name = get_bloginfo( 'name' );

		if ( false !== strpos( $blog_name, '.' ) ) {
			$parts = explode( '.', $blog_name );

			if ( 2 === count( $parts ) ) {
				$blogname = sprintf( '<span class="light">%1$s</span><span class="dot">.</span><span class="dark">%2$s</span>',
					esc_html( $parts[0] ),
					esc_html( $parts[1] )
				);
			}

			preg_match( '/\((.*)\)\s/', $blogname, $matches );

			if ( ! empty( $matches[1] ) ) {
				$blogname = str_replace( $matches[0], '<span class="exp">' . $matches[1] . '<span class="dot">.</span></span>', $blogname );
			}
		}

		if ( empty( $blogname ) ) {
			$blogname = esc_html( $blog_name );
		}

		/**
		 * Filter here to edit the displayed site name.
		 *
		 * @since  1.0.0
		 *
		 * @param  string $blogname The site name.
		 */
		return apply_filters( 'clustertheme_get_blogname', $blogname );
	}

/**
 * Dequeue scripts and styles for the maintenance page.
 *
 * @since  1.0.0
 */
function clustertheme_dequeue_scripts() {
	if ( ! clustertheme_is_maintenance() ) {
		return;
	}

	$queue_js = wp_scripts()->queue;

	if ( $queue_js ) {
		foreach ( $queue_js as $js_handle ) {
			wp_dequeue_script( $js_handle );
		}
	}

	$queue_css = wp_styles()->queue;

	if ( $queue_css ) {
		foreach ( $queue_css as $css_handle ) {
			if ( false !== array_search( $css_handle, array( 'clustertheme-maintenance', 'clustertheme-dark', 'clustertheme-red' ) ) ) {
				continue;
			}

			wp_dequeue_style( $css_handle );
		}
	}
}

/**
 * Maintenance head tag.
 *
 * Removes unneeded actions from wp_head() before using it.
 *
 * @since 1.0.0
 */
function clustertheme_maintenance_head() {
	if ( ! clustertheme_is_maintenance() ) {
		return;
	}

	global $wp_filter;

	$allowed = array(
		'_wp_render_title_tag',
		'wp_enqueue_scripts',
		'wp_resource_hints',
		'noindex',
		'print_emoji_detection_script',
		'wp_print_styles',
		'wp_print_head_scripts',
	);

	if ( isset( $wp_filter['wp_head']->callbacks ) ) {
		foreach ( $wp_filter['wp_head']->callbacks as $key => $hooks ) {
			foreach ( $hooks as $kh => $hp ) {
				if ( in_array( $kh, $allowed, true ) ) {
					continue;
				}

				remove_action( 'wp_head', $kh, $key );
			}
		}
	}

	add_action( 'wp_enqueue_scripts', 'clustertheme_dequeue_scripts', 1000 );

	wp_head();

	remove_action( 'wp_enqueue_scripts', 'clustertheme_dequeue_scripts', 1000 );
}

/**
 * Catches the WordPress loop for a later use.
 *
 * @since  1.0.0
 *
 * @return bool True if there are posts to display. False otherwise.
 */
function clusterpress_have_posts() {
	$clustertheme = clustertheme();

	if ( ! isset( $clustertheme->have_entries ) ) {
		$clustertheme->have_entries = have_posts();
	}

	return $clustertheme->have_entries;
}

/**
 * Output the Post thumbnail, if it exists.
 *
 * @since  1.0.0
 *
 * @param  string  $size The size for the post thumbnail. Default 'post-thumbnail'.
 * @param  boolean $link Whether to wrap the Thumbnail with a link to the post. Default true.
 * @param  string  $fit  Whether to check if the thumbnail is a square to eventually use a the given size.
 * @return string        HTML Output.
 */
function clustertheme_post_thumbnail( $size = 'post-thumbnail', $link = true, $fit = '' ) {
	if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
		return;
	}

	$clustertheme = clustertheme();

	if ( is_singular() && ! is_front_page() ) :
		// Add a temporary filter intercept the attachment object
		add_filter( 'wp_get_attachment_image_attributes', 'clustertheme_get_thumbnail_credit', 10, 2 );
	?>

	<div class="post-thumbnail">
		<?php the_post_thumbnail( $size ); ?>

		<?php if ( ! empty( $clustertheme->thumbnail_overlay ) ) : ?>
			<small class="post-thumbnail-overlay">
				<?php echo apply_filters( 'get_the_excerpt', $clustertheme->thumbnail_overlay ) ; ?>
			</small>
		<?php endif ; ?>
	</div><!-- .post-thumbnail -->

	<?php
		// Remove the filter we used to intercept the attachment object
		remove_filter( 'wp_get_attachment_image_attributes', 'clustertheme_get_thumbnail_credit', 10, 2 );

	else : // End is_singular()

		if ( $link ) : ?>
			<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
		<?php endif;

			if ( $fit ) {
				list( $url, $width, $height ) = wp_get_attachment_image_src( get_post_thumbnail_id(), $size );

				// Use medium for squares.
				if ( $width === $height ) {
					$size = $fit;
				}
			}

			the_post_thumbnail( $size, array( 'alt' => the_title_attribute( 'echo=0' ) ) );

		if ( $link ) : ?>
			</a>
		<?php endif ;

	endif; // End other cases
}

/**
 * Output a front page's panel thumbnail.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function clustertheme_front_page_thumbnail() {
	if ( ! has_post_thumbnail() ) {
		return;
	}
	?>
	<div class="front-page-thumbnail">
		<?php clustertheme_post_thumbnail( 'large', false, 'medium' ); ?>
	</div>
	<?php
}

/**
 * Prints HTML with meta information for the current post-date/time, tags category and author.
 *
 * @since  1.0.0
 *
 * @return  string HTML Output.
 */
function clustertheme_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( 'Publié le %s', 'post date', 'clustertheme' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'par %s', 'post author', 'clustertheme' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	// Defaults to nothing.
	$cat = $tag = '';

	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'clustertheme' ) );
		if ( $categories_list && clustertheme_categorized_blog() ) {
			$cat = sprintf( '<span class="cat-links">' . esc_html__( 'dans %1$s', 'clustertheme' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'clustertheme' ) );
		if ( $tags_list ) {
			$tag = sprintf( ' <span class="tags-links">' . esc_html__( 'et étiqueté %1$s.', 'clustertheme' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	printf( '<span class="posted-on">%1$s</span> <span class="byline">%2$s</span> %3$s%4$s',
		$posted_on,
		$byline,
		$cat,
		$tag
	); // WPCS: XSS OK.
}

/**
 * Output the ClusterTheme specific Excerpt.
 *
 * @since 1.0.0.
 *
 * @param  string $class Optional. A css class to add to the HTML tag.
 * @param  string $length The callback function to use to get the max length of the excerpt.
 * @return string The excerpt output.
 */
function clustertheme_excerpt( $class = '', $length = 'clustertheme_excerpt_length' ) {
	$clustertheme = clustertheme();

	// Remove the wpautop filter for our custom excerpt
	remove_filter( 'the_excerpt', 'wpautop' );

	if ( ! empty( $class ) ) {
		$class = ' class="' . sanitize_html_class( $class ) . '"';
	}

	if ( has_excerpt() ) : ?>
		<p<?php echo $class; ?>><?php the_excerpt(); ?>

	<?php else :
		add_filter( 'excerpt_more', 'clustertheme_excerpt_more' );

		if ( ! empty( $length ) ) {
			add_filter( 'excerpt_length', $length );
		}
	?>

		<p<?php echo $class; ?>><?php the_excerpt();?>

	<?php
		remove_filter( 'excerpt_more', 'clustertheme_excerpt_more' );

		if ( ! empty( $length ) ) {
			remove_filter( 'excerpt_length', $length );
		}

	endif;

	if ( has_excerpt() || empty( $clustertheme->excerpt_was_trimed ) ) :
		echo apply_filters( 'clustertheme_excerpt_readmore', sprintf( '
			<a class="readmore" href="%1$s" title="%2$s">%3$s &rarr;</a>
			',
			esc_url( get_permalink() ),
			esc_attr( get_the_title() ),
			esc_html__( 'Poursuivre la lecture', 'clustertheme' )
		) );

	endif;
	$clustertheme->excerpt_was_trimed = false; ?>

	</p>

	<?php
	// Restore the wpautop filter
	add_filter( 'the_excerpt', 'wpautop' );
}

/**
 * Prints HTML with action links about the post to add comments, edit it or any other action about the post.
 *
 * @since  1.0.0
 *
 * @return  string HTML Output.
 */
function clustertheme_entry_footer() {
	if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		if ( ! is_single() ) {
			echo '<span class="comments-link"><span class="dashicons dashicons-admin-comments"></span>';
			/* translators: %s: post title */
			comments_popup_link( sprintf( wp_kses( __( 'Commenter<span class="screen-reader-text"> %s</span>', 'clustertheme' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
			echo '</span>';
		} else {
			post_reply_link( array(
				'before' => '<span class="comment-reply-link"><span class="dashicons dashicons-admin-comments"></span>',
				'after'  => '</span>'
			) );
		}
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Modifier %s', 'clustertheme' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link"><span class="dashicons dashicons-edit"></span>',
		'</span>'
	);

	$links = array( sprintf( '
		<span class="twitter-share">
			<span class="dashicons dashicons-twitter"></span>
			<a href="https://twitter.com/intent/tweet?original_referer=%1$s&amp;source=tweetbutton&amp;text=%2$s&amp;url=%1$s&amp;via=ClusterPress" class="twitter" title="%3$s" target="_blank">%3$s</a>
		</span>
		', urlencode( get_permalink() ), urlencode( get_the_title() ), esc_html__( 'Tweeter', 'clustertheme' )
	) );

	if ( clustertheme_is_main_site() ) {
		$links[] =  sprintf( '
			<span class="paypal-support">
				<span class="clustertheme-icons clustertheme-icons-paypal"></span>
				<a href="https://www.paypal.me/imath" class="paypal" title="%1$s" target="_blank">%1$s</a>
			</span>
			', esc_attr__( 'Supporter', 'clustertheme' )
		);
	}

	/**
	 * Filter here to add/remove custom links.
	 *
	 * @since  1.0.0
	 *
	 * @param array $links the list of custom links to output.
	 */
	$footer_links = apply_filters( 'clustertheme_extra_entry_footer', $links );

	if ( ! empty( $footer_links ) && is_array( $footer_links ) ) {
		echo join( '', $footer_links );
	}
}

/**
 * Returns true if the site has more than 1 category.
 *
 * @since  1.0.0
 *
 * @return bool True if there is at least more than one category in use. False otherwise.
 */
function clustertheme_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'clustertheme_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'clustertheme_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so clustertheme_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so clustertheme_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in clustertheme_categorized_blog.
 *
 * @since  1.0.0
 */
function clustertheme_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'clustertheme_categories' );
}

/**
 * Customize the login screen look and feel.
 *
 * @since 1.0.0
 *
 * @return string CSS Outut.
 */
function clustertheme_login_screen_logo() {
	$logo_icon_url = get_site_icon_url( 84 );

	$logo_rule = '';
	if ( ! empty( $logo_icon_url ) ) {
		$logo_rule = sprintf( '
			#login h1 a {
				background-image: none, url(%s);
			}
		', esc_url_raw( $logo_icon_url ) );
	}

	$color_rules = array(
		'default' => '#8e3d58',
		'dark'    => '#615175',
		'red'     => '#af1E2d',
	);

	// Defaults to black
	$color_rule = '#23282d';

	$colorscheme = get_theme_mod( 'colorscheme', 'default' );

	// Load the selected colorscheme.
	if ( isset( $color_rules[ $colorscheme ] ) ) {
		$color_rule = $color_rules[ $colorscheme ];
	}

	wp_add_inline_style( 'login', sprintf( '
		%1$s

		#login p.submit .button-primary.button-large {
			color: #FFF;
			background-color: %2$s;
			border-color: %2$s;
			-webkit-box-shadow: none;
			box-shadow: none;
			text-shadow: none;
		}

		#login p.submit .button-primary.button-large:hover {
			color: %2$s;
			background-color: #FFF;
			border-color: %2$s;
		}

		a:focus {
			color: %2$s;
			-webkit-box-shadow: none;
			box-shadow: none;
		}

		#login input[type="text"]:focus,
		#login input[type="password"]:focus {
			border-color: %2$s;
			-webkit-box-shadow: none;
			box-shadow: none;
		}
	', $logo_rule, $color_rule ) );
}

/**
 * Add Sharing cards to single posts/pages
 *
 * @since 1.0.0
 *
 * @return string Facbook OG & Twitter meta tags.
 */
function clustertheme_sharing_cards() {
	if ( ! clustertheme_print_sharing_cards() || clusterpress_is_email_preview() ) {
		return;
	}

	$post = get_post();

	if ( ! empty( $post->post_excerpt ) ) {
		$description = strip_shortcodes( $post->post_excerpt );
	} elseif ( ! empty( $post->post_content ) ) {
		$description = strip_shortcodes( $post->post_content );
	} else {
		$description = get_bloginfo( 'description' );
	}

	$title       = wp_strip_all_tags( apply_filters( 'the_title', $post->post_title, $post->ID ) );
	$description = wp_strip_all_tags( apply_filters( 'the_excerpt', wp_trim_words( $description, 40, '...' ) ) );

	$metas = array(
		'twitter:card'        => 'summary_large_image',
		'twitter:site'        => '@ClusterPress',
		'twitter:title'       => $title,
		'twitter:description' => $description,
		'og:url'              => esc_url_raw( get_permalink( $post ) ),
		'og:type'             => esc_attr( get_post_type( $post ) ),
		'og:title'            => $title,
		'og:description'      => $description,
	);

	$large_image = get_the_post_thumbnail_url( $post );

	$metas = apply_filters( 'clustertheme_sharing_cards', $metas );

	if ( empty( $metas['twitter:title'] ) || empty( $metas['twitter:description'] ) ) {
		return;
	}

	if ( empty( $large_image ) && empty( $metas['twitter:image'] ) ) {
		$metas['twitter:card'] = 'summary';

		$small_image = get_site_icon_url( 120 );

		if ( ! empty( $small_image ) ) {
			$metas['twitter:image'] = esc_url_raw( $small_image );
			$metas['og:image']      = esc_url_raw( $small_image );
		}
	} elseif ( empty( $metas['twitter:image'] ) ) {
		$metas['twitter:image'] = esc_url_raw( $large_image );
		$metas['og:image']      = esc_url_raw( $large_image );
	}

	foreach ( $metas as $meta_name => $meta_content ) {
		printf( '<meta name="%1$s" content="%2$s">' . "\n", esc_attr( $meta_name ), esc_attr( $meta_content ) );
	}
}

/**
 * Print the email css inside the sent email.
 *
 * @since 1.0.0
 *
 * @return string CSS Output.
 */
function clustertheme_print_email_css() {
	$clustertheme = clustertheme();

	/**
	 * Filter here to replace the base email stylerules.
	 *
	 * @since 1.0.0
	 *
	 * @param string Absolute file to the css file.
	 */
	$email_css = apply_filters( 'clustertheme_get_email_css', sprintf( '%1$semail%2$s.css',
		trailingslashit( clustertheme_css_dir() ),
		clustertheme_js_css_suffix()
	) );

	// Directly insert it into the email template.
	if ( $email_css && file_exists( $email_css ) ) {
		include( $email_css );
	}

	$clustertheme->email_preferences = array_map( 'get_theme_mod', array(
		'disable_email_logo'      => 'disable_email_logo',
		'colorscheme'             => 'colorscheme',
		'email_header_line_color' => 'email_header_line_color',
		'email_body_text_color'   => 'email_body_text_color',
		'email_body_link_color'   => 'email_body_link_color',
	) );

	$clustertheme->email_preferences['default_color'] = clustertheme_get_scheme_hex_color();
	$link_color = $clustertheme->email_preferences['email_body_link_color'];

	if ( ! $link_color ) {
		$link_color = $clustertheme->email_preferences['default_color'];
	}

	// Add css overrides for the links
	if ( '#8e3d58' !== $link_color ) {
		printf( '
			a,
			a:hover,
			a:visited,
			a:active {
				color: %s;
			}
		', esc_attr( $link_color ) );
	}

	// Add css overrides for the text color of the header
	if ( '#8e3d58' !== $clustertheme->email_preferences['default_color'] ) {
		printf( '
			.container-padding.header span.dark,
			.container-padding.header span.exp,
			.container-padding.header span.dot {
				color: %s;
			}
		', esc_attr( $clustertheme->email_preferences['default_color'] ) );
	}
}

/**
 * Output the text color for the header's under line.
 *
 * @since  1.0.0
 *
 * @return string CSS Output.
 */
function clustertheme_email_line_color() {
	$clustertheme = clustertheme();

	$color = '#8e3d58';

	if ( ! empty( $clustertheme->email_preferences['email_header_line_color'] ) ) {
		$color = $clustertheme->email_preferences['email_header_line_color'];
	} elseif ( ! empty( $clustertheme->email_preferences['default_color'] ) ) {
		$color = $clustertheme->email_preferences['default_color'];
	}

	echo esc_attr( $color );
}

/**
 * Output the text color for the email body.
 *
 * @since  1.0.0
 *
 * @return string CSS Output.
 */
function clustertheme_email_text_color() {
	$clustertheme = clustertheme();

	$color = '#555555';

	if ( ! empty( $clustertheme->email_preferences['email_body_text_color'] ) ) {
		$color = $clustertheme->email_preferences['email_body_text_color'];
	}

	echo esc_attr( $color );
}

/**
 * Get the local field label.
 *
 * @since 1.0.0
 *
 * @return string the local field label.
 */
function clustertheme_locale_label() {
	return esc_html_e( 'Selectionnez votre langue de préférence pour le site :', 'clustertheme' );
}

/**
 * Output the local options for the locale dropdown.
 *
 * @since 1.0.0
 *
 * @param  array  $args Specific args if needed.
 * @return string HTML Output.
 */
function clustertheme_locale_options( $args = array() ) {
	echo join( "\n", clustertheme_get_locale_options( $args ) );
}

	/**
	 * Get the local options for the locale dropdown.
	 *
	 * @since 1.0.0
	 *
	 * @param  array  $args Specific args if needed.
	 * @return string The local option tags.
	 */
	function clustertheme_get_locale_options( $args = array() ) {
		$r = wp_parse_args( $args, array(
			'selected' => get_locale(),
		) );

		$languages          = clustertheme_get_available_locale();
		$languages          = array_fill_keys( $languages, 'English (United States)' );
		$languages['fr_FR'] = 'Français';
		$selected           = $r['selected'];

		$options = array();

		foreach ( $languages as $kl => $vl ) {
			$options[] = sprintf( '<option value="%1$s" %2$s>%3$s</option>',
				esc_attr( $kl ),
				selected( $selected, $kl, false ),
				esc_html( $vl )
			);
		}

		return apply_filters( 'clustertheme_get_locale_options', $options, $languages, $r );
	}

/**
 * Output or return the external profiles menu.
 *
 * @since 1.0.0
 *
 * @param  bool   $echo True to output. False to return.
 * @return string       HTML Output.
 */
function clustertheme_external_profiles( $echo = true ) {
	return wp_nav_menu( array(
		'theme_location' => 'footer-menu',
		'menu_class'     => 'profile-links-menu',
		'depth'          => 1,
		'link_before'    => '<span class="screen-reader-text">',
		'link_after'     => '</span>',
		'echo'           => $echo,
	) );
}

/**
 * Output the site footer links.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function clustertheme_site_footer_links() {
	$footer_links = clustertheme_get_site_footer_links();

	if ( ! $footer_links ) {
		return;
	}

	echo join( '<span class="sep"> | </span>', clustertheme_get_site_footer_links() );
}

	/**
	 * Get the site footer links.
	 *
	 * @since 1.0.0
	 *
	 * @return array The list of footer links to output.
	 */
	function clustertheme_get_site_footer_links() {
		$footer_links = array(
			sprintf( '
				%1$s <a href="%2$s"><span class="screen-reader-text">WordPress</span><img src="%3$s" class="site-footer-image" border="0"></a>
				',
				esc_html__( 'Fièrement propulsé par', 'clustertheme' ),
				esc_url( __( 'https://fr.wordpress.org/', 'clustertheme' ) ),
				esc_url( admin_url( 'images/wordpress-logo.svg' ) )
			),
			sprintf( '
				%s <a href="https://imathi.eu"><span class="screen-reader-text">imath</span><img src="//www.gravatar.com/avatar/8b208ca408dad63888253ee1800d6a03?s=24" class="site-footer-image" border="0"></a>
				',
				esc_html__( 'Thème : ClusterTheme par', 'clustertheme' )
			),
		);

		$tos = get_theme_mod( 'tos_' . clustertheme_locale() );

		if ( $tos ) {
			$page = get_page( $tos );

			if ( ! empty( $page->ID ) ) {
				$footer_links[] = sprintf( '<a href="%1$s">%2$s</a>',
					esc_url( get_permalink( $page ) ),
					esc_html( $page->post_title )
				);
			}
		}

		// This is only used on the main site.
		if ( clustertheme_is_main_site() ) {
			if ( 'fr' === clustertheme_locale() ) {
				$flag   = '🇬🇧';
				$locale = 'en_US';
				$text   = 'Use the english version of the site';
			} else {
				$flag   = '🇫🇷';
				$locale = 'fr_FR';
				$text   = 'Utiliser la version française du site';
			}

			array_unshift( $footer_links, sprintf( '
				<a href="%1$s"><span class="screen-reader-text">%2$s</span>%3$s</a>
				',
				esc_url( add_query_arg( 'locale', $locale ) ),
				$text,
				$flag
			) );
		}

		return apply_filters( 'clustertheme_get_site_footer_links', $footer_links );
	}
