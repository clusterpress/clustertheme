<?php
/**
 * Uprade/Install the Theme.
 *
 * @package ClusterTheme\inc
 * @subpackage upgrade
 *
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Upgrade the theme db version
 *
 * @since  1.0.0
 */
function clustertheme_upgrade() {
	if ( is_customize_preview() ) {
		return;
	}

	$db_version = get_option( 'clustertheme_version', 0 );
	$version    = clustertheme_version();

	if ( ! version_compare( $db_version, $version, '<' ) ) {
		return;
	}

	$email_post_id = (int) get_option( 'clustertheme_email_id', 0 );

	if ( ! $email_post_id ) {
		$email_post_id = wp_insert_post( array(
			'comment_status' => 'closed',
			'ping_status'    => 'closed',
			'post_status'    => 'private',
			'post_title'     => __( 'Modèle d\'email', 'clustertheme' ),
			'post_type'      => 'clustertheme_email',
			'post_content'   => sprintf( '<p>%1$s</p><p>%2$s</p><p>%3$s</p>',
				__( 'Vous pouvez personnaliser le gabarit qui sera utilisé pour les emails envoyés par WordPress.', 'clustertheme' ),
				__( 'Pour cela utilisez la barre latérale pour spécifier vos préférences.', 'clustertheme' ),
				__( 'Voici comment seront affichés les <a href="#">liens</a> contenus dans certains emails.', 'clustertheme' )
			),
		) );

		update_option( 'clustertheme_email_id', $email_post_id );
	}

	do_action( 'clustertheme_upgrade', $db_version, $version );

	// Update version.
	update_option( 'clustertheme_version', $version );
}
add_action( 'admin_init', 'clustertheme_upgrade', 1000 );
