<?php
/**
 * Multisite specific functions.
 *
 * @package ClusterTheme\inc
 * @subpackage ms
 *
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Check if the WP_Query can be altered.
 *
 * @since 1.0.0
 *
 * @param  WP_Query $wq The main WP_Query object.
 * @return bool         True if the WP_Query can be altered. False otherwise.
 */
function clustertheme_can_alter_posts_query( WP_Query $wq = null ) {
	$return = true;

	if ( ! $wq->is_main_query() || true === $wq->get( 'suppress_filters' ) ) {
		$return = false;
	}

	if ( $return && is_admin() && ! wp_doing_ajax() ) {
		$return = false;
	}

	return $return;
}

/**
 * Set the signup step.
 *
 * @since 1.0.0
 *
 * @return string|bool The signup step. False if not a signup step.
 */
function clustertheme_ms_is_signup_area() {
	$clustertheme = clustertheme();

	if ( ! isset( $clustertheme->signup_step ) ) {
		$uri   = parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH );
		$parts = explode( '/', trim( $uri, '/' ) );
		$page = array_pop( $parts );

		if ( 'wp-signup.php' === $page || 'wp-activate.php' === $page ) {
			$clustertheme->signup_step = str_replace( array( 'wp-', '.php' ), '', $page );
		} else {
			$clustertheme->signup_step = false;
		}
	}

	return $clustertheme->signup_step;
}

/**
 * Check if current page is a signup step.
 *
 * @since 1.0.0
 *
 * @return string|bool The signup step. False if not a signup step.
 */
function clusterpress_ms_signup_step() {
	$clustertheme = clustertheme();
	$return = false;

	if ( isset( $clustertheme->signup_step ) ) {
		$return = $clustertheme->signup_step;
	}

	return $return;
}

/**
 * Shortcircuit the WP Query if the page requested is the signup or activate one.
 *
 * IMHO WordPress shouldn't query posts for nothing in these two cases.
 *
 * @since  1.0.0
 *
 * @param  null   $return   A null value to use the regular WP Query.
 * @param  WP_Query $wq     The WP Query object.
 * @return null|array       Null if not in a Signup area.
 *                          An array containing a ClusterTheme Post otherwise.
 */
function clustertheme_posts_pre_query( $return = null, WP_Query $wq ) {
	if ( ! clustertheme_can_alter_posts_query( $wq ) || ! clustertheme_ms_is_signup_area() ) {
		return $return;
	}

	// Set the post
	$post_title = __( 'Créer un compte', 'clustertheme' );
	if ( 'activate' === clustertheme()->signup_step ) {
		$post_title = __( 'Activer un compte', 'clustertheme' );
	}

	$wq = clustertheme_alter_posts_query( $wq, array(
		'post_type'      => 'clustertheme_signup',
		'post_title'     => $post_title,
	) );

	if ( isset( $wq->clustertheme_null ) || empty( $wq->posts ) ) {
		unset( $wq->clustertheme_null );
		return $return;
	}

	return $wq->posts;
}
add_filter( 'posts_pre_query', 'clustertheme_posts_pre_query', 12, 2 );

/**
 * Register the signup Javascript.
 *
 * This script is a way to avoid spams.
 *
 * @since 1.0.0
 */
function clustertheme_ms_register_js( $scripts = array() ) {
	return array_merge(
		$scripts,
		array(
			'clustertheme-signup' => array(
				'file'   => '%1$ssignup%2$s.js',
				'url'    => clustertheme_js_url(),
				'deps'   => array( 'jquery' ),
				'footer' => true,
			),
		)
	);
}
add_filter( 'clustertheme_register_js', 'clustertheme_ms_register_js', 10, 1 );

/**
 * Enqueur the signup Javascript.
 *
 * @since 1.0.0
 */
function clustertheme_ms_enqueue_js() {
	if ( ! clusterpress_ms_signup_step() ) {
		return;
	}

	wp_enqueue_script( 'clustertheme-signup' );
}
add_action( 'clustertheme_enqueue_cssjs', 'clustertheme_ms_enqueue_js' );

/**
 * Add signup fields to the signup form.
 *
 * @since 1.0.0
 *
 * @param null|WP_Error $errors Null or the WordPress error object
 * @return string HTML Output.
 */
function clustertheme_ms_signup_extra_fields( $errors = null ) {
	// Set requested language if any.
	$args = array();
	if ( ! empty( $_REQUEST['locale'] ) ) {
		$args['selected'] = $_REQUEST['locale'];
	}

	/**
	 * Hook here if you need to add signup fields before the locale dropdown.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Error|null Null or a WP_Error object containing signup errors.
	 */
	do_action( 'clustertheme_before_locale_signup_field', $errors );

	// Output the locale dropdown.
	?>

	<label for="user_locale"><?php clustertheme_locale_label(); ?></label>
	<select name="locale" id="user_locale">

		<?php clustertheme_locale_options( $args ); ?>

	</select>

	<?php
	/**
	 * Hook here if you need to add signup fields after the locale dropdown.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Error|null Null or a WP_Error object containing signup errors.
	 */
	do_action( 'clustertheme_after_locale_signup_field', $errors );

	if ( get_theme_mod( 'tos_' . clustertheme_locale() ) ) : ?>

		<label for="tos_accepted">
			<input type="checkbox" id="tos_accepted" name="tos_accepted" value="1">
			<?php printf( esc_html__( 'J\'ai pris connaissance et m\'engage à respecter les %s.', 'clustertheme' ),
				sprintf( '<a href="#rules">%s</a>', esc_html__( 'règles du jeu', 'clustertheme' ) )
			); ?>
		</label>
		<?php
		if ( $errmsg = $errors->get_error_message( 'tos_accepted' ) ) {
			echo '<p class="error">' . $errmsg . '</p>';
		}

	endif;
}
add_action( 'signup_extra_fields', 'clustertheme_ms_signup_extra_fields', 10, 1 );

/**
 * Add the Terms of service page under the signup form if needed.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function clustertheme_ms_signup_tos() {
	global $post;
	$reset_post = $post;

	if ( ! did_action( 'signup_extra_fields' ) ) {
		return;
	}

	$page_id = get_theme_mod( 'tos_' . clustertheme_locale() );

	if ( empty( $page_id ) ) {
		return;
	}

	$page = get_page( $page_id );

	if ( empty( $page->ID ) ) {
		return;
	}

	$post = $page;
	setup_postdata( $page->ID );
	?>
	<div class="mu_register wp-signup-container">
		<h2 id="rules"><?php the_title(); ?></h2>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content">

				<?php the_content(); ?>

			</div>

			<br class="clear">
		</article>
	</div>

	<?php
	wp_reset_postdata();
	$post = $reset_post;
}
add_action( 'after_signup_form', 'clustertheme_ms_signup_tos' );

/**
 * Extra signup validation rules.
 *
 * @since 1.0.0
 *
 * @param  array $result The validation results.
 * @return array         The validation results.
 */
function clustertheme_ms_validate_signup( $result = array() ) {
	if ( function_exists( 'get_current_screen' ) ) {
		$current_screen = get_current_screen();
	}

	// If on an admin screen do not check the signup email.
	if ( ! empty( $current_screen->id ) ) {
		return $result;
	}

	$errors = $result['errors'];

	if ( ! is_wp_error( $errors ) || $errors->get_error_code() ) {
		return $result;
	}

	if ( get_theme_mod( 'tos_' . clustertheme_locale() ) && empty( $_POST['tos_accepted'] ) ) {
		$result['errors']->add(
			'tos_accepted',
			__( 'Vous devez accepter les règles du jeu.', 'clustertheme' )
		);
	}

	if ( empty( $_POST['_clustertheme_courriel'] ) ) {
		$errors->add(
			'user_email',
			__( 'Une erreur est survenue. Merci de vérifier votre email.', 'clustertheme' )
		);

		$result['errors'] = $errors;
	} else {
		unset( $_POST['_clustertheme_courriel'] );
	}

	return apply_filters( 'clustertheme_ms_validate_signup', $result );
}
add_filter( 'wpmu_validate_user_signup', 'clustertheme_ms_validate_signup', 10, 1 );

/**
 * Add custom Signup metas on user's registration
 *
 * @since 1.0.0
 *
 * @param  array $metas The signup metas.
 * @return array        The signup metas.
 */
function clustertheme_ms_signup_meta( $metas = array() ) {
	$metas['clustertheme'] = array();

	if ( ! empty( $_POST['locale'] ) ) {
		$metas['clustertheme']['locale'] =  sanitize_text_field( $_POST['locale'] );
	}

	if ( ! empty( $_POST['tos_accepted'] ) ) {
		$metas['clustertheme']['tos_accepted'] =  (int) $_POST['tos_accepted'];
	}

	return $metas;
}
add_filter( 'add_signup_meta', 'clustertheme_ms_signup_meta' );

/**
 * Add custom user metas on user's activation
 *
 * @since 1.0.0
 *
 * @param int   $user_id  User ID.
 * @param int   $password User password.
 * @param array $meta     Signup meta data.
 */
function clustertheme_ms_signup_meta_set_signup_meta( $user_id = 0, $password = '', $metas = array() ) {
	if ( empty( $metas['clustertheme'] ) || empty( $user_id ) ) {
		return;
	}

	foreach ( $metas['clustertheme'] as $meta_key => $meta_value ) {
		if ( 'locale' === $meta_key ) {
			update_user_meta( $user_id, $meta_key, $meta_value );
		} elseif ( 'tos_accepted' === $meta_key ) {
			continue;
		} else {
			add_user_meta( $user_id, $meta_key, $meta_value );
		}
	}
}
add_action( 'wpmu_activate_user', 'clustertheme_ms_signup_meta_set_signup_meta', 10, 3 );
