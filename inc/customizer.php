<?php
/**
 * ClusterTheme Theme Customizer.
 *
 * @package ClusterTheme\inc
 * @subpackage customizer
 *
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Add custom settings for the Theme Customizer.
 *
 * @since  1.0.0
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function clustertheme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport          = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport   = 'postMessage';

	/**
	 * Custom colors.
	 */
	$wp_customize->add_setting( 'colorscheme', array(
		'default'           => 'default',
		'transport'         => 'refresh',
		'sanitize_callback' => 'clustertheme_sanitize_colorscheme',
	) );

	$wp_customize->add_control( 'colorscheme', array(
		'type'    => 'radio',
		'label'    => __( 'Gestion des couleurs', 'clustertheme' ),
		'choices'  => array(
			'default' => __( 'Raisin', 'clustertheme' ),
			'dark'    => __( 'Raisin noir', 'clustertheme' ),
			'red'     => __( 'Raisin rouge', 'clustertheme' ),
		),
		'section'  => 'colors',
		'priority' => 5,
	) );

	/**
	 * Theme options.
	 */
	$wp_customize->add_section( 'theme_options', array(
		'title'    => __( 'Options du thème', 'clustertheme' ),
		'priority' => 130, // Before Additional CSS.
	) );

	// Maintenance mode
	$wp_customize->add_setting( 'maintenance_mode', array(
		'default'           => 0,
		'sanitize_callback' => 'absint',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( 'maintenance_mode', array(
		'label'       => __( 'Maintenance', 'clustertheme' ),
		'section'     => 'theme_options',
		'type'        => 'radio',
		'choices'     => array(
			0 => __( 'Pas de maintenance.', 'clustertheme' ),
			1 => __( 'Maintenance en cours.', 'clustertheme' ),
		),
	) );

	// English description
	$wp_customize->add_setting( 'tagline', array(
		'default'           => '',
		'sanitize_callback' => 'clustertheme_sanitize_tagline',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( 'tagline', array(
		'label'           => __( 'Tagline', 'clustertheme' ),
		'description'     => __( 'Choisissez le slogan à utiliser pour l\'accueil de vos visiteurs anglais', 'clustertheme' ),
		'section'         => 'theme_options',
		'active_callback' => 'clustertheme_is_main_site',
	) );

	// English landing page
	$wp_customize->add_setting( 'landing_page', array(
		'default'           => 0,
		'sanitize_callback' => 'absint',
		'transport'         => 'refresh',
	) );

	$wp_customize->add_control( 'landing_page', array(
		'label'           => __( 'Landing page', 'clustertheme' ),
		'description'     => __( 'Choisissez la page à afficher pour l\'accueil de vos visiteurs anglais', 'clustertheme' ),
		'section'         => 'theme_options',
		'type'            => 'dropdown-pages',
		'allow_addition'  => true,
		'active_callback' => 'clustertheme_has_static_front_page',
	) );

	// Terms of service in French
	$wp_customize->add_setting( 'tos_fr', array(
		'default'           => 0,
		'sanitize_callback' => 'absint',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( 'tos_fr', array(
		'label'           => __( 'Page des règles d\'utilisation', 'clustertheme' ),
		'section'         => 'theme_options',
		'type'            => 'dropdown-pages',
		'allow_addition'  => true,
		'active_callback' => 'clustertheme_is_main_site',
	) );

	// Terms of service in English
	$wp_customize->add_setting( 'tos_en', array(
		'default'           => 0,
		'sanitize_callback' => 'absint',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( 'tos_en', array(
		'label'           => __( 'Page for the terms of service.', 'clustertheme' ),
		'section'         => 'theme_options',
		'type'            => 'dropdown-pages',
		'allow_addition'  => true,
		'active_callback' => 'clustertheme_is_main_site',
	) );

	/**
	 * Theme email template.
	 */
	$wp_customize->add_section( 'theme_email', array(
		'title'    => __( 'Modèle d\'email', 'clustertheme' ),
		'priority' => 125, // Before Theme options.
	) );

	// Allow the admin to disable the email logo
	$wp_customize->add_setting( 'disable_email_logo', array(
		'default'           => 0,
		'sanitize_callback' => 'absint',
		'transport'         => 'refresh',
	) );

	$wp_customize->add_control( 'disable_email_logo', array(
		'label'           => __( 'Intégrer le logo du site dans l\'email', 'clustertheme' ),
		'section'         => 'theme_email',
		'type'            => 'radio',
		'choices'         => array(
			0 => __( 'Oui', 'clustertheme' ),
			1 => __( 'Non', 'clustertheme' ),
		),
		'active_callback' => 'clustertheme_has_custom_logo',
	) );

	// Allow the admin to customize the header's under line color.
	$wp_customize->add_setting( 'email_header_line_color', array(
		'default'           => clustertheme_get_scheme_hex_color(),
		'sanitize_callback' => 'sanitize_hex_color',
		'transport'         => 'refresh',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'email_header_line_color', array(
		'label'       => __( 'Couleur de soulignement de l\'entête', 'clustertheme' ),
		'section'     => 'theme_email',
	) ) );

	// Allow the admin to customize the links color.
	$wp_customize->add_setting( 'email_body_link_color', array(
		'default'           => clustertheme_get_scheme_hex_color(),
		'sanitize_callback' => 'sanitize_hex_color',
		'transport'         => 'refresh',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'email_body_link_color', array(
		'label'       => __( 'Couleur des liens', 'clustertheme' ),
		'section'     => 'theme_email',
	) ) );

	// Allow the admin to customize the text color.
	$wp_customize->add_setting( 'email_body_text_color', array(
		'default'           => '#555555',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport'         => 'refresh',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'email_body_text_color', array(
		'label'       => __( 'Couleur du texte', 'clustertheme' ),
		'section'     => 'theme_email',
	) ) );
}

/**
 * Colorscheme sanitization function.
 *
 * @since 1.0.0
 *
 * @param string $input The name of the scheme.
 * @param string        The sanitized name of the scheme.
 */
function clustertheme_sanitize_colorscheme( $input ) {
	$valid = array( 'default', 'dark', 'red' );

	if ( in_array( $input, $valid, true ) ) {
		return $input;
	}

	return 'default';
}

/**
 * Is there a static front page displaying home.
 *
 * @since 1.0.0
 *
 * @return bool True if a static front page is displayed as home. False otherwise.
 */
function clustertheme_has_static_front_page() {
	return 'page' === get_option( 'show_on_front' ) && clustertheme_is_main_site();
}

/**
 * Colorscheme sanitization function.
 *
 * @since 1.0.0
 *
 * @param string $input The tagline.
 * @param string        The sanitized tagline.
 */
function clustertheme_sanitize_tagline( $input ) {
	$has_filter = has_filter( 'pre_option_blogdescription', 'clustertheme_blogdescription' );

	// Temporarly remove the localization filter
	if ( $has_filter ) {
		remove_filter( 'pre_option_blogdescription', 'clustertheme_blogdescription', 10, 1 );
	}

	$sanitized = sanitize_option( 'blogdescription', $input );

	// Restore the localization filter
	if ( $has_filter ) {
		add_filter( 'pre_option_blogdescription', 'clustertheme_blogdescription', 10, 1 );
	}

	return $sanitized;
}

/**
 * Is there a custom logo for the site ?
 *
 * @since 1.0.0.
 *
 * @return bool True if a custom logo is activated. False otherwise.
 */
function clustertheme_has_custom_logo() {
	return (bool) has_custom_logo();
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 *
 * @since 1.0.0
 */
function clustertheme_customize_preview_js() {
	$min = clustertheme_js_css_suffix();

	wp_enqueue_script( 'clustertheme-customizer-preview', get_template_directory_uri() . "/js/customizer-preview{$min}.js", array( 'customize-preview' ), clustertheme_version(), true );
}

/**
 * Load our custom controle JS File and global vars.
 *
 * @since 1.0.0
 */
function clustertheme_customize_control_js() {
	$min = clustertheme_js_css_suffix();

	wp_enqueue_script ( 'clustertheme-customizer-control', get_template_directory_uri() . "/js/customizer{$min}.js", array(), clustertheme_version(), true );
	wp_localize_script( 'clustertheme-customizer-control', 'clustertheme', array(
		'emailUrl' => esc_url_raw( get_permalink( get_option( 'clustertheme_email_id' ) ) ),
	) );
}
