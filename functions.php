<?php
/**
 * Main Class to Bootstrap the Theme.
 *
 * Author: @imath
 * URL: imathi.eu
 * Requires WordPress 4.7
 *
 * @package ClusterTheme
 * @subpackage functions
 *
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Cluster Theme Bootstrap class
 *
 * @since  1.0.0
 */
final class ClusterTheme {
	/**
	 * Instance of this class.
	 *
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * Required WordPress version for the theme
	 *
	 * @var string
	 */
	public static $wp_version = 4.7;

	/**
	 * Initialize the theme
	 *
	 * @since  1.0.0
	 */
	private function __construct() {
		$this->globals();
		$this->inc();
		$this->set_user_locale();
		$this->hooks();
		$this->supports();
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since  1.0.0
	 */
	public static function start() {

		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Set some globals for the theme
	 *
	 * @since  1.0.0
	 */
	private function globals() {
		$this->version = '1.0.0';

		// Directories
		$this->dir         = get_template_directory();
		$this->inc_dir     = trailingslashit( $this->dir ) . 'inc';
		$this->widgets_dir = trailingslashit( $this->dir ) . 'widgets';
		$this->js_dir      = trailingslashit( $this->dir ) . 'js' ;
		$this->css_dir     = trailingslashit( $this->dir ) . 'css';

		// Urls
		$this->url     = get_template_directory_uri();
		$this->js_url  = trailingslashit( $this->url ) . 'js' ;
		$this->css_url = trailingslashit( $this->url ) . 'css';

		// Theme specific globals
		$this->content_width          = 800;
		$this->requires_wp_upgrade    = ! isset( $GLOBALS['wp_version'] ) || (float) $GLOBALS['wp_version'] < self::$wp_version;
		$this->is_maintenance         = (bool) get_theme_mod( 'maintenance_mode', 0 );
		$this->is_clusterpress_active = function_exists( 'clusterpress' );
		$this->is_main_site           = (int) get_current_blog_id() === (int) get_current_network_id();
	}

	/**
	 * Set the best language for the visitor.
	 *
	 * @since  1.0.0
	 */
	public function set_user_locale() {
		// Only apply the user locale on the root site and for WordPress > 4.7.
		if ( ! $this->is_main_site || $this->requires_wp_upgrade ) {
			return;
		}

		$user_id     = get_current_user_id();
		$supported   = array( 'fr_FR' => 'fr' , 'en_US' => 'en' );
		$user_locale = '';

		// Try the url query vars.
		if ( ! empty( $_GET['locale'] ) && isset( $supported[ $_GET['locale'] ] ) ) {
			$user_locale = $_GET['locale'];

			if ( $user_id ) {
				update_user_meta( $user_id, 'locale', $user_locale );
			} else {
				$secure = is_ssl();
				setcookie( 'user_locale', $user_locale, 0, COOKIEPATH, COOKIE_DOMAIN, $secure );

				if ( SITECOOKIEPATH != COOKIEPATH ) {
					setcookie( 'user_locale', $user_locale, 0, SITECOOKIEPATH, COOKIE_DOMAIN, $secure );
				}
			}

		// Try the user setting.
		} elseif ( $user_id ) {
			$user_locale = get_user_meta( $user_id, 'locale', true );
		}

		// The user locale is still unknown
		if ( ! $user_locale ) {

			// Try the cookie store
			if ( ! empty( $_COOKIE['user_locale'] ) ) {
				$user_locale = $_COOKIE['user_locale'];

			// Try the server accepted languages
			} else {
				$server_locale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
				$server_locale = explode( ',', $server_locale );
				$server_locale = substr( strtolower( reset( $server_locale ) ), 0, 2 );

				$is_supported = array_search( $server_locale, $supported );

				if ( $is_supported ) {
					$user_locale = $is_supported;
				} else {
					$user_locale = 'en_US';
				}
			}
		}

		// If the user local is different than the site's one, switch locale.
		if ( ! empty( $user_locale ) && $user_locale !== get_locale() ) {
			switch_to_locale( $user_locale );
		}
	}

	/**
	 * Include required files
	 *
	 * @since  1.0.0
	 */
	private function inc() {
		// Bye! You should upgrade!
		if ( $this->requires_wp_upgrade ) {
			return;
		}

		// Custom functions
		require_once( $this->inc_dir . '/functions.php' );

		// Custom template tags for this theme.
		require_once(  $this->inc_dir . '/tags.php' );

		// Customizer additions.
		require_once( $this->inc_dir . '/customizer.php' );

		// User nav Widget
		require_once( $this->widgets_dir . '/ct-user-nav.php' );

		// Admin only functions
		if ( is_admin() ) {
			require_once( $this->inc_dir . '/upgrade.php' );
		}

		// Multisite with registrations enabled
		if ( is_multisite() && true === (bool) get_option( 'users_can_register', false ) ) {
			require_once( $this->inc_dir . '/ms.php' );
		}

		// ClusterPress specific functions
		if ( $this->is_clusterpress_active ) {
			require_once( $this->inc_dir . '/clusterpress.php' );

			// The ClusterPress Navigation Widget is only used on subsites.
			if ( ! cp_is_main_site() && cp_cluster_is_enabled( 'doc' ) ) {
				require_once( $this->widgets_dir . '/cp-doc-toc.php' );
			}
		}

		// Maintenance page
		if ( ! is_admin() && $this->is_maintenance ) {
			require_once( $this->inc_dir . '/maintenance.php' );

			$this->maintenance = ClusterTheme_Maintenance::start();
		}
	}

	/**
	 * Setup actions and filters.
	 *
	 * @since  1.0.0
	 */
	private function hooks() {
		add_action( 'posts_results',            'clustertheme_append_front_page_panels',   10, 2 );

		add_action( 'edit_category',            'clustertheme_category_transient_flusher'        );

		add_action( 'save_post',                'clustertheme_category_transient_flusher'        );

		add_filter( 'frontpage_template',       'clustertheme_front_page_template'               );

		add_action( 'wp_enqueue_scripts',       'clustertheme_register_cssjs',              1    );

		add_action( 'wp_enqueue_scripts',       'clustertheme_enqueue_cssjs',              10    );

		add_action( 'widgets_init',             'clustertheme_widgets_init'                      );

		add_action( 'login_init',               'clustertheme_login_screen_logo'                 );

		add_action( 'wp_head',                  'clustertheme_extra_header'                      );

		add_action( 'wp_head',                  'clustertheme_sharing_cards',              20    );

		add_filter( 'body_class',               'clustertheme_body_classes'                      );

		add_filter( 'walker_nav_menu_start_el', 'clustertheme_nav_menu_attributes',        10, 4 );

		add_filter( 'wp_trim_words',            'clustertheme_excerpt_was_trimed',         10, 1 );

		add_filter( 'comment_class',            'clustertheme_comment_class',              10, 4 );

		add_filter( 'site_icon_image_sizes',    'clustertheme_login_logo_size',            10, 1 );

		if ( 'en' === clustertheme_locale() && $this->is_main_site )  {
			add_filter( 'pre_option_page_on_front',   'clustertheme_page_on_front',   10, 1 );
			add_filter( 'pre_option_blogdescription', 'clustertheme_blogdescription', 10, 1 );
		}

		add_filter( 'bloginfo', 'clustertheme_setup_description', 10, 2 );

		// Email

		add_action( 'init',           'clustertheme_register_email_type'       );

		add_action( 'phpmailer_init', 'clustertheme_email',              10, 1 );

		// Customizer

		add_action( 'customize_register',                 'clustertheme_customize_register'   );

		add_action( 'customize_preview_init',             'clustertheme_customize_preview_js' );

		add_action( 'customize_controls_enqueue_scripts', 'clustertheme_customize_control_js' );
	}

	/**
	 * Setup the supported features.
	 *
	 * @since 1.0.0
	 */
	private function supports() {
		// Localisation Support
		load_theme_textdomain( 'clustertheme', get_template_directory() . '/languages' );

		if ( $this->requires_wp_upgrade ) {
			add_action( 'admin_notices', array( $this, 'warning' ) );

			// No need to carry on.
			return;
		}

		// Enables post and comment RSS feed links to head
		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 800, 400, true );

		// Title tag
		add_theme_support( 'title-tag' );

		// Custom Logo
		add_theme_support( 'custom-logo', array(
			'height'      => 60,
			'width'       => 60,
			'flex-height' => false,
			'flex-width'  => false,
		) );

		// Add Menu Support
		add_theme_support( 'menus' );

		// Menu
		register_nav_menus( clustertheme_get_nav_menus( $this->is_main_site ) );

		// Load Custom styles inside the wp editor
		add_editor_style( clusterpress_get_editor_styles() );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Gist Support
		wp_embed_register_handler( 'clustertheme_gist', '#(https://gist.github.com/(.*?)/([a-zA-Z0-9]+)?)(\#file(\-|_)(.+))?$#i', 'clustertheme_gist_handler' );

		// Shortcodes
		add_shortcode( 'clustertheme_profiles', 'clustertheme_profiles_shortcode' );
		add_shortcode( 'clustertheme_icons',    'clustertheme_icons_shortcode'    );

		if ( $this->is_main_site ) {
			add_shortcode( 'clustertheme_user_actions', 'clustertheme_user_actions_shortcode' );
		}

		$GLOBALS['content_width'] = apply_filters( 'clustertheme_content_width', $this->content_width );
	}

	/**
	 * Display an upgrade warning only once.
	 *
	 * @since  1.0.0
	 */
	public function warning() {
		if ( did_action( 'clustertheme_warning_displayed' ) ) {
			return;
		}

		printf(
			'<div id="message" class="error"><p>%s</p></div>',
			sprintf( __( 'ClusterTheme nécessite la version %s de WordPress, merci de mettre à jour WordPress.', 'clustertheme' ), self::$required_wp_version )
		);

		do_action( 'clustertheme_warning_displayed' );
	}
}

/**
 * Start the ClusterTheme
 *
 * @since  1.0.0
 *
 * @return ClusterTheme The Theme main instance.
 */
function clustertheme() {
	return ClusterTheme::start();
}
add_action( 'after_setup_theme', 'clustertheme' );
