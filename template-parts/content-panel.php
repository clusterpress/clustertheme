<?php
/**
 * Template part for displaying panel content in front-page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ClusterTheme\template-parts
 * @subpackage content-panel
 *
 * @since 1.0.0
 */

$panel_index = clustertheme_front_page_panel_index();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $panel_index ); ?>>
	<div class="page_container">
		<header class="entry-header">
			<?php if ( 'panel1' === $panel_index ) :

				the_title( '<h1 class="entry-title" id="' . $panel_index . '">', '</h1>' );

			else :

				the_title( '<h2 class="entry-title" id="' . $panel_index . '">', '</h2>' );

			endif ; ?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php
				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Poursuivre la lecture %s <span class="meta-nav">&rarr;</span>', 'clustertheme' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
			?>
		</div><!-- .entry-content -->

		<?php clustertheme_front_page_thumbnail(); ?>
	</div>
</article><!-- #post-## -->
