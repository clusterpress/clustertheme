<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ClusterTheme\template-parts
 * @subpackage content
 *
 * @since 1.0.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if ( ! is_single() ) :

			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );

		endif;

		if ( 'post' === get_post_type() ) : ?>

			<div class="entry-meta">
				<?php clustertheme_posted_on(); ?>
			</div><!-- .entry-meta -->

		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php clustertheme_post_thumbnail(); ?>

	<div class="entry-content">
		<?php

			if ( ! is_single() ) :
				clustertheme_excerpt();

			else :
				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Poursuivre la lecture %s <span class="meta-nav">&rarr;</span>', 'clustertheme' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
			endif;

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages :', 'clustertheme' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php clustertheme_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
