<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ClusterTheme\template-parts
 * @subpackage content-none
 *
 * @since 1.0.0
 */

?>

<section class="no-results not-found">
	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'Prêt à publier votre premier article ? <a href="%1$s">Commencez par ici</a>.', 'clustertheme' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_html_e( 'Désolé, aucun contenu ne correspond à votre recherche. Merci de réessayer avec d\'autres mots clés.', 'clustertheme' ); ?></p>
			<?php
				get_search_form();

		else : ?>

			<p><?php esc_html_e( 'Il sembre que nous ne parvenions pas à trouver ce contenu. Il est possible qu\'une recherche vous aide.', 'clustertheme' ); ?></p>
			<?php
				get_search_form();

		endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
