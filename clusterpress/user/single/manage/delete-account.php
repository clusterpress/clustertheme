<?php
/**
 * Clusthetheme's Single User Delete Account template.
 *
 * @package ClusterTheme\ClusterPress\user\single\manage
 * @subpackage delete-account
 *
 * @since 1.0.0
 */
?>

<form class="edit-user-account" method="post" action="">

	<?php
	clusterpress()->feedbacks->add_feedback(
		'user_account[delete]',
		esc_html__( 'Vous êtes sur le point de supprimer toutes vos informations personnelles. Il ne sera pas possible de revenir en arrière. Merci de confirmer que vous comprenez bien ces conséquences.', 'clustertheme' ),
		'info'
	);

	cp_get_template_part( 'assets/feedbacks' );
	?>

	<label for="confirm-deletion">
		<input type="checkbox" value="1" name="cp_delete_account[confirm]" id="confirm-deletion">
		<?php esc_html_e( 'J\'ai bien compris ces conséquences et confirme vouloir supprimer mon compte.', 'clustertheme' ); ?>
	</label>

	<?php cp_account_submit_button( 'cp_delete_account' ); ?>

</form>
