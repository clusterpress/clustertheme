<?php
/**
 * The template for displaying the user ClusterPress home page.
 *
 * @package ClusterTheme\ClusterPress\user\single
 * @subpackage home
 *
 * @since 1.0.0
 */
?>

<h2><?php cp_user_profile_the_loop_title(); ?></h2>

<?php if ( clustertheme_user_has_fields() ) : ?>

	<div class="view-user-profile">

		<?php while ( cp_user_the_fields() ) : cp_user_the_field() ; ?>

			<?php if ( cp_user_has_description() ) : ?>

				<blockquote><?php cp_user_field_value(); ?></blockquote>

			<?php elseif ( cp_user_has_field_value() ) : ?>

				<p class="value"><?php cp_user_field_value(); ?></p>
				<p class="label"><?php cp_user_field_name(); ?></p>

			<?php endif ; ?>

		<?php endwhile ; ?>

	</div>

<?php else :

	cp_user_no_profile_found();

endif; ?>
