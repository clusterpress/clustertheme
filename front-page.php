<?php
/**
 * The template for displaying the front page.
 *
 * @package ClusterTheme
 * @subpackage front-page
 *
 * @since 1.0.0
 */

get_header(); ?>

<div id="primary" class="content-area wrapper">
	<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'panel' );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer();
