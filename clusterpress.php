<?php
/**
 * The template for displaying all ClusterPress pages.
 *
 * @package ClusterTheme
 * @subpackage clusterpress
 *
 * @since 1.0.0
 */

get_header(); ?>

	<div id="headline">
		<div class="wrapper">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</div>

	<div class="wrapper">

		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				<div class="wrapper">

					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'page' );

					endwhile; // End of the loop.
					?>

				</div><!-- .wrapper -->
			</main><!-- #main -->
		</div><!-- #primary -->

		<?php get_sidebar( 'clusterpress' ); ?>

	</div><!-- .wrapper -->

<?php get_footer();
