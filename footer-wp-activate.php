<?php
/**
 * The template for displaying the activate footer.
 *
 * It uses the wp-signup footer
 *
 * @package ClusterTheme
 * @subpackage footer-wp-activate
 *
 * @since 1.0.0
 */

get_template_part( 'footer-wp-signup' );
