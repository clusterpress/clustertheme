<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ClusterTheme
 * @subpackage archive
 *
 * @since 1.0.0
 */

get_header(); ?>

<div id="headline">
	<div class="wrapper">
		<?php if ( clusterpress_have_posts() ) : ?>
			<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="archive-description">', '</div>' );
			?>
		<?php else : ?>
			<h1 class="page-title"><?php esc_html_e( 'Aucun contenu n\'a été trouvé', 'clustertheme' ); ?></h1>
		<?php endif; ?>
	</div>
</div>

<div class="wrapper">

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="wrapper">

			<?php if ( clusterpress_have_posts() ) :

				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_format() );

				endwhile;

				the_posts_navigation( clustertheme_navigation_args() );

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>

			</div><!-- .wrapper -->
		</main><!-- #main -->
	</div><!-- #primary -->

	<?php get_sidebar(); ?>
</div><!-- .wrapper -->

<?php get_footer();
