<?php
/**
 * The template for displaying the ClusterTheme's footer.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ClusterTheme
 * @subpackage footer
 *
 * @since 1.0.0
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="wrapper">

			<?php if ( has_nav_menu( 'footer-menu' ) ) : ?>
				<nav class="profiles-navigation" role="navigation" aria-label="<?php _e( 'Profils externes', 'clustertheme' ); ?>">

					<?php clustertheme_external_profiles(); ?>

				</nav><!-- .profiles-navigation -->
			<?php endif; ?>

			<div class="site-info">
				<?php clustertheme_site_footer_links(); ?>
			</div><!-- .site-info -->
		</div><!-- .wrapper -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
