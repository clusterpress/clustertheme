( function( $ ) {

	$( document ).ready( function() {
		$( '#user_email' ).css( {
			'display' : 'none'
		} ).before( $( '<input></input>' )
			.prop( 'id', 'clustertheme-courriel' )
			.prop( 'name', '_clustertheme_courriel' )
			.attr( 'type', 'email' )
			.css( {
				'width'    : '100%',
				'font-size': '24px',
				'margin'   : '5px 0'
			} )
		);

		$( '#setupform' ).on( 'submit', function( event ) {
			$( '#user_email' ).val( $( '#clustertheme-courriel' ).val() );

			return event;
		} );
	} );

} )( jQuery );
