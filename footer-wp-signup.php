<?php
/**
 * The template for displaying the ClusterTheme's signup footer.
 *
 * @package ClusterTheme
 * @subpackage footer-wp-signup
 *
 * @since 1.0.0
 */
?>

				</main><!-- #main -->
			</div><!-- .wrapper -->
		</div><!-- #primary -->

	<?php get_template_part( 'footer' );
