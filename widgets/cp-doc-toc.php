<?php
/**
 * ClusterTheme's ClusterPress Doc widget.
 *
 * @package ClusterTheme\widgets
 * @subpackage cp-doc-toc
 *
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'CP_Doc_Toc_Widget' ) ) :

/**
 * A widget to display the table of content of a ClusterPress Doc.
 *
 * @since  1.0.0
 */
class CP_Doc_Toc_Widget extends WP_Widget {

 	/**
	 * Set up the widget instance.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		$widget_ops = array(
			'description' => esc_html__( 'La table des matière d\'une ressource documentaire.', 'clustertheme' ),
			'classname'   => 'cp-doc-toc-container',
		);

		parent::__construct( 'cp_doc_toc', esc_html__( '(CP) Table des matière', 'clustertheme' ), $widget_ops );

		add_action( 'wp_enqueue_scripts', array( $this, 'add_inline_style' ) );
	}

	/**
	 * Register the widget
	 *
	 * @since 1.0.0
	 */
	public static function register_widget() {
		register_widget( 'CP_Doc_Toc_Widget' );
	}

	/**
	 * Style the pending counts.
	 *
	 * @since  1.0.0
	 */
	public function add_inline_style() {
		wp_add_inline_style( 'clusterpress-doc', '
			#secondary .widget.cp-doc-toc-container {
				padding: 1em 0;
				margin: inherit;
				margin-bottom: 2em;
				float: none;
				width: 100%;
			}

			#secondary .widget.cp-doc-toc-container h2.widget-title {
				padding-left: 1em;
				font-size: 100%;
			}
		' );
	}

	/**
	 * Display the content of the widget
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $args
	 * @param  array  $instance
	 */
	public function widget( $args = array(), $instance = array() ) {
		if ( ! function_exists( 'cp_is_doc' ) || ! cp_is_doc() ) {
			return;
		}

		// No table of content, stop!
		if ( ! cp_current_doc_has_toc() ) {
			return;
		}

		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		if ( empty( $title ) ) {
			$title = __( 'Table des matières', 'clustertheme' );
		}

		echo $args['before_widget'];

		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		cp_doc_the_doc_toc();

		echo $args['after_widget'];
	}

	/**
	 * Save the new instance.
	 *
	 * @since  1.0.0
	 *
	 * @param  array $new_instance The instance containing updated values.
	 * @param  array $old_instance The instance containing old values
	 * @return array               The updated instance.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['title'] = '';
		if ( ! empty( $new_instance['title'] ) ) {
			$instance['title'] = strip_tags( $new_instance['title'] );
		}

		return $instance;
	}

	/**
	 * Display the form in Widgets Administration
	 *
	 * @since 1.0.0
	 *
	 * @param array $instance The Widget instance
	 */
	public function form( $instance = array() ) {
		//Defaults
		$instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Titre:', 'clustertheme' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		<?php
	}
}

endif;

add_action( 'widgets_init', array( 'CP_Doc_Toc_Widget', 'register_widget' ), 11 );
