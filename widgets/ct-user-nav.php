<?php
/**
 * ClusterTheme's User Nav widget.
 *
 * @package ClusterTheme\inc
 * @subpackage cp-user-profile-nav
 *
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'CT_User_Nav_Widget' ) ) :

/**
 * A widget to display the current user's profile nav.
 *
 * @since  1.0.0
 */
class CT_User_Nav_Widget extends WP_Widget {

 	/**
	 * Set up the widget instance.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		$this->is_registration_open = (bool) get_option( 'users_can_register', false );

		if ( true === $this->is_registration_open ) {
			$description = esc_html__( 'Une navigation dynamique pour afficher les liens d\'inscription, de connexion, de déconnexion et d\'accès au profil de l\'utilisateur connecté.', 'clustertheme' );
		} else {
			$description = esc_html__( 'Une navigation dynamique pour afficher les liens de connexion, de déconnexion et d\'accès au profil de l\'utilisateur connecté.', 'clustertheme' );
		}

		$widget_ops = array( 'description' => $description );
		parent::__construct( 'ct_user_nav', esc_html__( '(CT) Navigation Utilisateur', 'clustertheme' ), $widget_ops );

		add_action( 'wp_enqueue_scripts', array( $this, 'add_inline_style' ) );
	}

	/**
	 * Style the pending counts.
	 *
	 * @since  1.0.0
	 */
	public function add_inline_style() {
		wp_add_inline_style( 'clustertheme-main', sprintf( '
			.widget_ct_user_nav .pending-count {
				display: inline-block;
				background-color: %1$s;
				color: #fff;
				font-size: 9px;
				line-height: 17px;
				font-weight: 600;
				margin: 2px;
				vertical-align: middle;
				-webkit-border-radius: 10px;
				border-radius: 10px;
				z-index: 26;
				padding: 0 6px;
				float: right;
			}

			#secondary .widget.widget_ct_user_nav ul.subitems li {
				border:none;
				margin-left: 2em;
				padding-bottom: 0;
			}

			#secondary .widget.widget_ct_user_nav ul.subitems li a {
				width: %2$s;
				display: block;
			}
		', clustertheme_get_scheme_hex_color(), '100%' ) );
	}

	/**
	 * Register the widget
	 *
	 * @since 1.0.0
	 */
	public static function register_widget() {
		register_widget( 'CT_User_Nav_Widget' );
	}

	/**
	 * Get the user URL.
	 *
	 * @since 1.0.0
	 *
	 * @return string The user URL.
	 */
	public static function get_user_url() {
		$profile_url = false;

		$user = wp_get_current_user();

		if ( empty( $user->ID ) ) {
			return $profile_url;
		}

		if ( current_user_can( 'read' ) ) {
			$profile_url = get_edit_profile_url( $user->ID );
		} elseif ( is_multisite() ) {
			$profile_url = get_dashboard_url( $user->ID, 'profile.php' );
		}

		return apply_filters( 'clustertheme_user_profile_url', $profile_url, $user );
	}

	/**
	 * Display the content of the widget
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $args
	 * @param  array  $instance
	 */
	public function widget( $args = array(), $instance = array() ) {
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		if ( empty( $title ) ) {
			$title = __( 'Navigation utilisateur', 'clustertheme' );

			if ( ! is_user_logged_in() ) {
				$title = '';
			}
		}

		if ( is_multisite() && ! clustertheme_is_main_site() ) {
			$urls = array( 'tribe' => array(
				'link'     => network_site_url(),
				'text'     => __( 'Retour sur le site de la tribu.', 'clustertheme' ),
				'classes' => array( 'clustertheme-icons', 'clustertheme-icons-cluster' ),
			) );
		} else {
			$urls = array();
		}

		$current_site_url = home_url();

		if ( ! is_user_logged_in() ) {

			// Reset the redirect url when on the main site.
			if ( clustertheme_is_main_site() ) {
				$current_site_url = '';
			}

			$urls['login'] = array(
				'link'     => wp_login_url( $current_site_url ),
				'text'     => __( 'Se connecter', 'clustertheme' ),
				'classes'  => array( 'dashicons', 'dashicons-admin-network' ),
			);

			if ( $this->is_registration_open ) {
				$urls['register'] = array(
					'link'     => wp_registration_url(),
					'text'     => __( 'Devenir membre', 'clustertheme' ),
					'classes' => array( 'dashicons', 'dashicons-nametag' ),
				);
			}

		} else {

			$urls['user'] = array(
				'link'     => $this->get_user_url(),
				'text'     => __( 'Mon Profil', 'clustertheme' ),
				'classes' => array( 'dashicons', 'dashicons-admin-users' ),
			);

			$urls['logout'] = array(
				'link'     => wp_logout_url( $current_site_url ),
				'text'     => __( 'Se déconnecter', 'clustertheme' ),
				'classes' => array( 'dashicons', 'dashicons-migrate' ),
			);
		}

		$urls = apply_filters( 'clustertheme_user_nav_urls', $urls );

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		} ?>
			<ul class="cp-dynamic-nav">

				<?php foreach ( $urls as $key => $url ) :
					$classes = '';
					if ( ! empty( $url['classes'] ) ) {
						$classes = ' class="' . join( ' ', array_map( 'sanitize_html_class', $url['classes'] ) ) . '"';
					}

					printf( '<li><a href="%1$s"><span%2$s></span> %3$s</a>',
						esc_url( $url['link'] ),
						$classes,
						esc_html( $url['text'] )
					);

					if ( ! empty( $url['subitems'] ) && is_array( $url['subitems'] ) ) : ?>
						<ul class="subitems">

							<?php foreach ( $url['subitems'] as $subitem ) {

								$count = '';
								if ( ! empty( $subitem['count'] ) ) {
									$count = sprintf( '<span class="pending-count">%d</span>', (int) $subitem['count'] );
								}

								$sub_classes = '';
								if ( ! empty( $subitem['classes'] ) ) {
									$sub_classes = ' class="' . join( ' ', array_map( 'sanitize_html_class', $subitem['classes'] ) ) . '"';
								}

								printf( '<li><a href="%1$s"><span%2$s></span> %3$s%4$s</a>',
									esc_url( $subitem['link'] ),
									$sub_classes,
									esc_html( $subitem['text'] ),
									$count
								);

							} ?>

						</ul>

					<?php endif ; ?>

					</li>

				<?php endforeach ; ?>

			</ul>

		<?php
		echo $args['after_widget'];
	}

	/**
	 * Save the new instance.
	 *
	 * @since  1.0.0
	 *
	 * @param  array $new_instance The instance containing updated values.
	 * @param  array $old_instance The instance containing old values
	 * @return array               The updated instance.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['title'] = '';
		if ( ! empty( $new_instance['title'] ) ) {
			$instance['title'] = strip_tags( $new_instance['title'] );
		}

		return $instance;
	}

	/**
	 * Display the form in Widgets Administration
	 *
	 * @since 1.0.0
	 *
	 * @param array $instance The Widget instance
	 */
	public function form( $instance = array() ) {
		//Defaults
		$instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Titre:', 'clustertheme' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		<?php
	}
}

endif;

add_action( 'widgets_init', array( 'CT_User_Nav_Widget', 'register_widget' ), 10 );
