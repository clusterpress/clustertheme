<?php
/**
 * The template for the ClusterPress's Maintenance page header.
 *
 * @package ClusterTheme
 * @subpackage header-maintenance
 *
 * @since 1.0.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php clustertheme_maintenance_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
