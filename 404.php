<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package ClusterTheme
 * @subpackage 404
 *
 * @since 1.0.0
 */

get_header(); ?>

<div id="headline">
	<div class="wrapper">
		<h1 class="page-title"><?php esc_html_e( 'Oops! cette page est introuvable.', 'clustertheme' ); ?></h1>
	</div>
</div>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="wrapper">
			<section class="error-404 not-found">

				<div class="page-content">
					<p><?php esc_html_e( 'Il semble que nous ne soyons pas parvenu à trouver votre contenu. Essayez les liens ci-après ou tentez de rechercher des mots clés.', 'clustertheme' ); ?></p>

					<?php
						get_search_form();

						the_widget( 'WP_Widget_Recent_Posts' );

						// Only show the widget if site has multiple categories.
						if ( clustertheme_categorized_blog() ) :
					?>

					<div class="widget widget_categories">
						<h2 class="widget-title"><?php esc_html_e( 'Catégories les plus utilisées', 'clustertheme' ); ?></h2>
						<ul>
						<?php
							wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
						?>
						</ul>
					</div><!-- .widget -->

					<?php
						endif;

						/* translators: %1$s: smiley */
						$archive_content = '<p>' . sprintf( esc_html__( 'Essayez de parcourir les archives mensuelles. %1$s', 'clustertheme' ), convert_smilies( ':)' ) ) . '</p>';
						the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );

						the_widget( 'WP_Widget_Tag_Cloud' );
					?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->
		</div><!-- .wrapper -->
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
