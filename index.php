<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ClusterTheme
 * @subpackage index
 *
 * @since 1.0.0
 */

get_header(); ?>

<div id="headline">
	<div class="wrapper">
		<?php if ( clusterpress_have_posts() ) :
			if ( is_home() && ! is_front_page() ) : ?>
				<h1 class="page-title"><?php single_post_title(); ?></h1>
			<?php else : ?>
				<h1 class="page-title"><?php esc_html_e( 'Actualités', 'clustertheme' ); ?></h1>
			<?php endif ; ?>
		<?php else : ?>
			<h1 class="page-title"><?php esc_html_e( 'Aucun contenu n\'a été trouvé', 'clustertheme' ); ?></h1>
		<?php endif; ?>
	</div>
</div>

<div class="wrapper">

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="wrapper">

			<?php if ( clusterpress_have_posts() ) :

				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_format() );

				endwhile;

				the_posts_navigation( clustertheme_navigation_args() );

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>

			</div><!-- .wrapper -->
		</main><!-- #main -->
	</div><!-- #primary -->

	<?php get_sidebar(); ?>
</div><!-- .wrapper -->

<?php get_footer();
